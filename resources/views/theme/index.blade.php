
@include('theme.cmn_head')
@section('content')

@endsection

<style>
  .main-header.style-two .header-top {
    background: #346bae;
}  
.about-section #content_block_three .inner-box{
  margin-left: 60px;
  margin-top: -140px;
  z-index: 2;
}

#content_block_three .inner-box{
  position: relative;
  background: #fff;
  padding: 50px 50px 60px 50px;
  border-radius: 5px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);
}

#content_block_three .inner-box h2{
  position: relative;
  display: block;
  text-align: center;
  font-size: 30px;
  line-height: 40px;
  font-weight: 700;
  color: #222;
  padding-bottom: 17px;
  margin-bottom: 42px;
}

#content_block_three .inner-box .form-inner .form-group{
  position: relative;
  margin-bottom: 18px;
}

#content_block_three .inner-box .form-inner .form-group input[type='text'],
#content_block_three .inner-box .form-inner .form-group input[type='email'],
#content_block_three .inner-box .form-inner .form-group select{
  position: relative;
  width: 100%;
  height: 55px;
  background: #f8f8f8;
  border: 3px solid #f8f8f8;
  border-radius: 30px;
  padding: 10px 25px;
  transition: all 500ms ease;
}

#content_block_three .inner-box .form-inner .form-group input:focus{
  
}

#content_block_three .inner-box .form-inner .form-group label{
  position: relative;
  display: block;
  font-size: 16px;
  color: #222;
  margin-bottom: 8px;
}

#content_block_three .inner-box .form-inner .form-group .btn-box button{
  position: relative;
  display: block;
  width: 100%;
  font-size: 16px;
  font-weight: 700;
  color: #222;
  background: transparent;
  border-radius: 30px;
  text-align: center;
  padding: 13px 30px;
  text-transform: uppercase;
  cursor: pointer;
  transition: all 500ms ease;
}

#content_block_three .inner-box .form-inner .form-group .btn-box button:hover{
  color: #fff;
}

#content_block_three .inner-box .form-inner .form-group .btn-box{
  position: relative;
  margin-top: 17px;
}

#content_block_three .inner-box .form-inner .form-group:last-child{
  margin-bottom: 0px;
}





.home-slider.slider-style-03 {
    padding: 0;
    width: 100%;
    height: 100%;
}
.home-slider.slider-style-03 .container-fluid, .home-slider.slider-style-03 .row {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
}
.home-slider.slider-style-03 .banner-video-area {
    width: 100%;
    height: 100%;
    position: relative;
    z-index: -1;
}
.home-slider.slider-style-03 #myVideo {
    min-width: 100%;
    min-height: 100%;
}
.home-slider.slider-style-03 .banner-video-area:after {
    background-color: rgba(0, 0, 0, 0.6);
    width: 100%;
    height: 100%;
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    z-index: 1;
}
.home-slider.slider-style-03 .slider-content-area {
    width: 100%;
}
.home-slider.slider-style-03 .slider-content-area .slider-content {
    width: 51%;
    padding-left: 9%;
    margin: 0;
    position: absolute;
    top: 3%;
    -webkit-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
}

.home-slider.slider-style-03 .slider-content-area .slider-content .slider-heading {
    color: #FFFFFF;
    margin-bottom: 50px;
}
.main-menu .navigation > li > a {
    color:#FFF;
}
.owl-carousel .owl-item img {
    display: block;
    width: 130px !important;
    height: 190px;
    margin: 0px auto;
}
</style>
<div class="home-slider slider-style-03">
            <div class="container-fluid">
                <div class="row">   
                    <div class="banner-video-area">
                        <video autoplay="" muted="" loop="" id="myVideo">
                            <source src="https://vote.localballot.in/bluelife/public/video.mp4" type="video/mp4">
                        </video>
                    </div>
                    <!--// Banner Video End-->
                    <div class="slider-content-area">
                        <div class="slider-content">
                            <img src="{{ asset('theme/images/caption.png') }}" class="img img-responsive"/>
                            <div class="slider-video">
                                
                            </div>                                           
                        </div>  
                    </div>   
                             
                </div>
            </div>
        </div>
        
    <section class="about-section1 hide" >
        
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                    <div id="content_block_two">
                        <div class="content-box">
                            <div class="sec-title"><h1>About Us</h1></div>
                            <div class="text">BlueLife is committed to achieve delivering Innovative, Unique, Quality Products and Quality Service Excellence in our mission to our customers. We shall approach this through continuously improving our work culture, approach and management style. We promote trust, homogeneity and believe in delivering products and service excellence to achieve customer delight.</div>
                            <div class="text">Elegant Customization - We absorb global and home trends with curiosity and customize our offerings with an elegant method to meet the customer’s wide range of expectations and care for his objects like precious pearls.</div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 inner-column">
                <figure class="image-box wow flipInY about-img" data-wow-delay="600ms" data-wow-duration="1500ms"><a href="{{ asset('theme/images/bluelife/TulipsRED/highlights.png')}}" class="lightbox-image" data-fancybox="gallery"><img src="{{ asset('theme/images/bluelife/TulipsRED/highlights.png')}}" alt=""></a></figure>
   
                </div>
            </div>
        </div>
    </section>


    <!-- feature-section -->
    <section class="feature-section bg-color-1 sec-pad">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h1>How we are unique</h1>
            </div>
            <div class="inner-content">
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                        <div class="feature-block-one wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <div class="icon-box wow slideInDown" data-wow-delay="250ms" data-wow-duration="1500ms"><i class="flaticon-drop-leaf-table"></i></div>
                                <h3><a href="#">Stainless-Steel Storage</a></h3>
                                <div class="text">The purified water is stored in a 304 grade stainless steel tank unlike in-built and 
non-detachable plastic tank models available in the market.  Being plastic a by-product of 
petroleum, possibility of water contamination with micro-plastic leaches is more. Also plastic 
storage containers undergo chemical changes under environmental changes such as heat and 
light. But, Stainless Steel is always the best, safe and hygienic under all conditions.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                        <div class="feature-block-one wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <div class="icon-box wow slideInDown" data-wow-delay="250ms" data-wow-duration="1500ms"><i class="flaticon-water"></i></div>
                                <h3><a href="#">Why Aluminium Enclosure</a></h3>
                                <div class="text">BlueLife water purifiers come with powder coated aluminium enclosures which are robust 
and more durable unlike other available plastic moulds in the market. BlueLife water 
purifiers will not react to the weather temperatures such as summer, hotness in the kitchens 
and maintains drinking water pure and hygienic in ambient temperature. Another idea is to 
avoid using plastic to the extent possible as it is believed that hygienically metal is better 
opted than plastic. Unlike all the existing models, the enclosure is designed to the easiest 
serviceability. </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                        <div class="feature-block-one wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <div class="icon-box wow slideInDown" data-wow-delay="250ms" data-wow-duration="1500ms"><i class="flaticon-teardrop"></i></div>
                                <h3><a href="#">Why Detachable Storage</a></h3>
                                <div class="text">BlueLife’s Unique detachable stainless-steel storage water purifiers allows the customer to 
Remove the tank, Clean it and Refill with purified water to keep your water hygienic until 
consumption.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                        <div class="feature-block-one wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <div class="icon-box wow slideInDown" data-wow-delay="250ms" data-wow-duration="1500ms"><i class="flaticon-water-barrel"></i></div>
                                <h3><a href="#">Digital RO Purification Process</a></h3>
                                <div class="text">India’s first microcontroller based digital integrated water purification process introduced by 
BlueLife, enhances the quality of purified water and retains drinking water in hygienic 
condition. User friendly LED indicators for Power On, Purification On, Tank Full, No Inlet 
Water and Filter Change Alarm. </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- feature-section end -->

     <!-- service-layout-1 -->
     <section class="service-layout-1 sec-pad">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h1>Our Services</h1>
            </div>
            <div class="row clearfix">
                <div class="col-lg-4 col-md-6 col-sm-12 service-block">
                    <div class="service-block-one wow flipInY" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/resource/service-1.jpg')}}" alt=""></a></figure>
                            <div class="lower-content">
                                <h3><a href="#">We Manufacture</a></h3>
                                <div class="text">Exercitation kamco sed laboris aliquip duis aute ure dolor laboret buica dolore magna aliqua duis aute irure.</div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 service-block">
                    <div class="service-block-one wow flipInY" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/resource/service-2.jpg')}}" alt=""></a></figure>
                            <div class="lower-content">
                                <h3><a href="#">We Sell</a></h3>
                                <div class="text">Exercitation kamco sed laboris aliquip duis aute ure dolor laboret buica dolore magna aliqua duis aute irure.</div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 service-block">
                    <div class="service-block-one wow flipInY" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/resource/service-3.jpg')}}" alt=""></a></figure>
                            <div class="lower-content">
                                <h3><a href="#">We Serve</a></h3>
                                <div class="text">Exercitation kamco sed laboris aliquip duis aute ure dolor laboret buica dolore magna aliqua duis aute irure.</div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- service-layout-1 end -->



    <!-- delivery-section -->
    <section class="delivery-section bg-color-1">
        <div class="border-shap">
            <div class="border-1" style="background-image: url({{ asset('theme/images/icons/border-1.png')}});"></div>
            <div class="border-2" style="background-image: url({{ asset('theme/images/icons/border-2.png')}});"></div>
        </div>
        <div class="auto-container">
            <div class="top-title clearfix">
                <div class="title-inner">
                    <div class="sec-title"><h1>Our Products</h1></div>
                </div>
                <div class="text-inner">
                    <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco exa consequat. Duis aute rue dolor prehendrit lorem ipsum sit amet consectetur adipisicing sed.</div>
                </div>
            </div>
            <div class="row inner-content">
                <div class="col-md-12 inline-block">
                    <div class="col-md-4 ">
                    <div class="single-shop-block">
                        <div class="inner-box">
                            <div class="border-one"></div>
                            <div class="border-two"></div>
                            <figure class="image-box"><img src="{{ asset('theme/images/bluelife/Emerald/Emerald-P1.png')}}" alt=""></figure>
                            <h3><a href="shop-details.php">Emerald</a></h3>
                            <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                            <div class="price">₹17.00 <del>₹29.50</del></div>
                            <div class="cart-btn"><a href="shop-details.php"><i class="flaticon-online-shop"></i>Add to Cart</a></div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-4 ">
                    <div class="single-shop-block">
                        <div class="inner-box">
                            <div class="border-one"></div>
                            <div class="border-two"></div>
                            <figure class="image-box"><img src="{{ asset('theme/images/bluelife/Aarwa/aarwa2.png')}}" alt=""></figure>
                            <h3><a href="shop-details.php">Aarwa</a></h3>
                            <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                            <div class="price">₹24.00 <del>₹29.50</del></div>
                            <div class="cart-btn"><a href="shop-details.php"><i class="flaticon-online-shop"></i>Add to Cart</a></div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-4 ">
                    <div class="single-shop-block">
                        <div class="inner-box">
                            <div class="border-one"></div>
                            <div class="border-two"></div>
                            <figure class="image-box"><img src="{{ asset('theme/images/bluelife/Emerald/Emerald-P1.png')}}" alt=""></figure>
                            <h3><a href="shop-details.php">Emerald</a></h3>
                            <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                            <div class="price">₹17.00 <del>₹29.50</del></div>
                            <div class="cart-btn"><a href="shop-details.php"><i class="flaticon-online-shop"></i>Add to Cart</a></div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-4 ">
                    <div class="single-shop-block">
                        <div class="inner-box">
                            <div class="border-one"></div>
                            <div class="border-two"></div>
                            <figure class="image-box"><img src="{{ asset('theme/images/bluelife/Aarwa/aarwa2.png')}}" alt=""></figure>
                            <h3><a href="shop-details.php">Aarwa</a></h3>
                            <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                            <div class="price">₹24.00 <del>₹29.50</del></div>
                            <div class="cart-btn"><a href="shop-details.php"><i class="flaticon-online-shop"></i>Add to Cart</a></div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-4 ">
                    <div class="single-shop-block">
                        <div class="inner-box">
                            <div class="border-one"></div>
                            <div class="border-two"></div>
                            <figure class="image-box"><img src="{{ asset('theme/images/bluelife/Emerald/Emerald-P1.png')}}" alt=""></figure>
                            <h3><a href="shop-details.php">Emerald</a></h3>
                            <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                            <div class="price">₹17.00 <del>₹29.50</del></div>
                            <div class="cart-btn"><a href="shop-details.php"><i class="flaticon-online-shop"></i>Add to Cart</a></div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-4 ">
                    <div class="single-shop-block">
                        <div class="inner-box">
                            <div class="border-one"></div>
                            <div class="border-two"></div>
                            <figure class="image-box"><img src="{{ asset('theme/images/bluelife/Aarwa/aarwa2.png')}}" alt=""></figure>
                            <h3><a href="shop-details.php">Aarwa</a></h3>
                            <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                            <div class="price">₹24.00 <del>₹29.50</del></div>
                            <div class="cart-btn"><a href="shop-details.php"><i class="flaticon-online-shop"></i>Add to Cart</a></div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- delivery-section end -->


    <section class="about-section">
        
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                    <div id="content_block_two">
                        <div class="content-box">
                            <div class="sec-title"><h1>Become a dealer</h1></div>
                            <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco exa consequat. Duis aute rue dolor prehendrit duis aute irure dolor in reprehend erit temp voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat no proident officia sed ipsum. Qui officia deserunt mol anim id est laborum. Sed ut perspiciatis unde</div>
                            <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco exa consequat. Duis aute rue dolor prehendrit duis aute irure dolor in reprehend erit temp voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat no proident officia sed ipsum. Qui officia deserunt mol anim id est laborum. Sed ut perspiciatis unde</div>
                            <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco exa consequat. Duis aute rue dolor prehendrit duis aute irure dolor in reprehend erit temp voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat no proident officia sed ipsum. Qui officia deserunt mol anim id est laborum. Sed ut perspiciatis unde</div>

                            <div class="bold-text">Got a question? Dive in …</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 inner-column hide">
                    <div id="content_block_three">
                        <div class="inner-box" style="margin-top:0px;">
                            <h2>Enquiry</h2>
                            <div class="form-inner">
                                <form method="post"  id="contact-form" class="order-form">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                            <label>Your Name</label>
                                            <input type="text" name="name" id="first_name" required="">
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 form-group">
                                            <label>Email</label>
                                            <input type="email" name="email" id="email" required="">
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 form-group">
                                            <label>Phone</label>
                                            <input type="text" name="phone" id="phone" required="">
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                            <label>Address</label>
                                            <input type="text" name="address" id="address" required="">
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                            <label>Message</label>
                                            <input type="text" class="h-120"  name="message" id="message" required="">
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                            <div class="btn-box">
                                                <button type="submit" id="contact">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- video-section -->
    <section class="video-section bg-color-1 sec-pad">
        <div class="auto-container">
            <div class="upper-content">
                <div id="video_block_one">
                    <div class="video-inner">
                        <div class="video-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms" style="background-image: url({{ asset('theme/images/background/video-1.jpg')}});">
                            <div class="video-btn">
                                <a href="https://www.youtube.com/watch?v=nfP5N9Yc72A&amp;t=28s" class="lightbox-image" data-caption=""><i class="flaticon-play-button-arrowhead"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top-title clearfix">
                    <div class="title-inner">
                        <div class="sec-title"><h1>Helping To Improve</h1></div>
                    </div>
                    <div class="text-inner">
                        <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco exa consequat. Duis aute rue dolor prehendrit lorem ipsum sit amet consectetur adipisicing sed.</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- video-section end -->


    <!-- composition-section -->
    <section class="composition-section ">
        
        <div class="auto-container">
            <div class="sec-title text-center">
                <h1>Bluelife Basic Water<br />Composition</h1>
            </div>
            <div class="upper-content">
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-12 col-sm-12 left-column">
                        <div class="inner-box">
                            <div class="single-item wow slideInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <figure class="icon-box">
                                    <span>K+</span>
                                    <img src="{{ asset('theme/images/icons/water-drop-1.png')}}" alt="">
                                </figure>
                                <h3><a href="#">Potassium</a></h3>
                                <h5>2.5 mg/L</h5>
                                <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                            </div>
                            <div class="single-item wow slideInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                                <figure class="icon-box">
                                    <span>Fl</span>
                                    <img src="{{ asset('theme/images/icons/water-drop-1.png')}}" alt="">
                                </figure>
                                <h3><a href="#">Fluoride</a></h3>
                                <h5>0.5 mg/L</h5>
                                <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 image-column">
                        <div class="image-box">
                            <div class="pattern-bg" style="background-image: url({{ asset('theme/images/icons/pattern-2.png')}});"></div>
                            <figure class="image wow slideInUp" data-wow-delay="0ms" data-wow-duration="1500ms"><img src="{{ asset('theme/images/bluelife/TulipsULTRA/TU.png')}}" alt=""></figure>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 right-column">
                        <div class="inner-box">
                            <div class="single-item wow slideInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <figure class="icon-box">
                                    <span>Cl</span>
                                    <img src="{{ asset('theme/images/icons/water-drop-1.png')}}" alt="">
                                </figure>
                                <h3><a href="#">Chloride</a></h3>
                                <h5>350a mg/L</h5>
                                <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                            </div>
                            <div class="single-item wow slideInRight" data-wow-delay="300ms" data-wow-duration="1500ms">
                                <figure class="icon-box">
                                    <span>Mg</span>
                                    <img src="{{ asset('theme/images/icons/water-drop-1.png')}}" alt="">
                                </figure>
                                <h3><a href="#">Magnesium</a></h3>
                                <h5>14.5 mg/L</h5>
                                <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lower-content clearfix">
                <div class="single-item wow slideInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <h3>Nitrates</h3>
                    <h5>2 mg/L</h5>
                </div>
                <div class="single-item wow slideInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
                    <h3>Bicarbonates</h3>
                    <h5>157 mg/L</h5>
                </div>
                <div class="single-item wow slideInUp" data-wow-delay="400ms" data-wow-duration="1500ms">
                    <h3>Sulphates</h3>
                    <h5>5.6 mg/L</h5>
                </div>
                <div class="single-item wow slideInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <h3>Sodium</h3>
                    <h5>0.4 mg/L</h5>
                </div>
            </div>
        </div>
    </section>
    <!-- composition-section end -->


   

    <!-- testimonial-section  -->
    <section class="testimonial-section bg-color-1">
        
        <div class="auto-container">
            <div class="top-title clearfix">
                <div class="title-inner">
                    <div class="sec-title"><h1>Our Testimonials</h1></div>
                </div>
                <div class="text-inner">
                    <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco exa consequat. Duis aute rue dolor prehendrit lorem ipsum sit amet consectetur adipisicing sed.</div>
                </div>
            </div>
            <div class="inner-content">
                <div class="client-testimonial-carousel owl-carousel owl-theme owl-dots-none">
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">Since vindictively over agile the some far well besides constructively well airy then close excellent grabbed gosh contrary far dalmatian upheld intrepid bought and toucan more some apart dear boa much cast falcon dwelled.</div>
                            <div class="author-info">
                                <h5 class="name">Brendon Taylor</h5>
                                <span class="designation">office delivery</span>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">Since vindictively over agile the some far well besides constructively well airy then close excellent grabbed gosh contrary far dalmatian upheld intrepid bought and toucan more some apart dear boa much cast falcon dwelled.</div>
                            <div class="author-info">
                                <h5 class="name">Brendon Taylor</h5>
                                <span class="designation">office delivery</span>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">Since vindictively over agile the some far well besides constructively well airy then close excellent grabbed gosh contrary far dalmatian upheld intrepid bought and toucan more some apart dear boa much cast falcon dwelled.</div>
                            <div class="author-info">
                                <h5 class="name">Brendon Taylor</h5>
                                <span class="designation">office delivery</span>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">Since vindictively over agile the some far well besides constructively well airy then close excellent grabbed gosh contrary far dalmatian upheld intrepid bought and toucan more some apart dear boa much cast falcon dwelled.</div>
                            <div class="author-info">
                                <h5 class="name">Brendon Taylor</h5>
                                <span class="designation">office delivery</span>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">Since vindictively over agile the some far well besides constructively well airy then close excellent grabbed gosh contrary far dalmatian upheld intrepid bought and toucan more some apart dear boa much cast falcon dwelled.</div>
                            <div class="author-info">
                                <h5 class="name">Brendon Taylor</h5>
                                <span class="designation">office delivery</span>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">Since vindictively over agile the some far well besides constructively well airy then close excellent grabbed gosh contrary far dalmatian upheld intrepid bought and toucan more some apart dear boa much cast falcon dwelled.</div>
                            <div class="author-info">
                                <h5 class="name">Brendon Taylor</h5>
                                <span class="designation">office delivery</span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Client Thumbs Carousel-->
                <div class="client-thumb-outer">
                    <div class="client-thumbs-carousel owl-carousel owl-theme owl-dots-none owl-nav-none">
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{ asset('theme/images/resource/testimonial-1.png')}}" alt=""></figure>
                        </div>
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{ asset('theme/images/resource/testimonial-2.png')}}" alt=""></figure>
                        </div>
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{ asset('theme/images/resource/testimonial-3.png')}}" alt=""></figure>
                        </div>
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{ asset('theme/images/resource/testimonial-1.png')}}" alt=""></figure>
                        </div>
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{ asset('theme/images/resource/testimonial-2.png')}}" alt=""></figure>
                        </div>
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{ asset('theme/images/resource/testimonial-3.png')}}" alt=""></figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- testimonial-section end -->


    <!-- clients-section  -->
    <section class="clients-section sec-pad hide">
        <div class="auto-container">
            <div class="top-title clearfix">
                <div class="title-inner">
                    <div class="sec-title"><h1>Trusted Partners</h1></div>
                </div>
                <div class="text-inner">
                    <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco exa consequat. Duis aute rue dolor prehendrit lorem ipsum sit amet consectetur adipisicing sed.</div>
                </div>
            </div>
            <div class="clients-carousel owl-carousel owl-theme owl-nav-none owl-dots-none">
                <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/clients/client-1.png')}}" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/clients/client-2.png')}}" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/clients/client-3.png')}}" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/clients/client-4.png')}}" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/clients/client-5.png')}}" alt=""></a></figure>
            </div>
        </div>
    </section>
    <!-- clients-section end -->
    @include('theme.cmn_footer')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
  
<script>
    $(document).ready(function() {
            
        var url = "{{ url('') }}" +
            '/api/client/products?limit=12&getCategory=1&getDetail=1&language_id=1&sortBy=id&sortType=DESC&currency=INR';
        appendTo = 'new-arrival';
        fetchProduct(url, appendTo);



        function fetchProduct(url, appendTo) {
            $.ajax({
                type: 'get',
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
                    clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
                },
                beforeSend: function() {},
                success: function(data) {
                    if (data.status == 'Success') {
                        const templ = document.getElementById("product-card-template");
                        var products = "";
                        console.log(data.data);
                        for (i = 0; i < data.data.length; i++) {

if(data.data[i].product_id >5 && data.data[i].product_id <13){}

                            var length = 120;
                            var url = "{{ URL('/product/')}}";
                            var imgpath = "{{ asset('')}}"+'/'+data.data[i]
                                        .product_gallary.detail[1].gallary_path;
                            products += '<div class="col-md-4"><div class="single-shop-block"><div class="inner-box"><div class="border-one"></div> <div class="border-two"></div><a href="'+url+'/'+
    data
    .data[i].product_id + '/' + data
    .data[i].product_slug+'"><figure class="image-box"><img src="'+imgpath+'" alt=""></figure></a>';
                            products += '<h3><a href="'+url+'/'+
                                    data
                                    .data[i].product_id + '/' + data
                                    .data[i].product_slug+'">'+data.data[i]
                                    .detail[0].title+'</a></h3><div class="text">'+data.data[i]
                                    .detail[0].desc.substring(0,length)+'..</div><div class="price">₹'+data.data[i]
                                    .product_discount_price+' <del>₹'+data.data[i]
                                    .product_price+'</del></div>';
                            
                        
                                if (data.data[i].product_type == 'simple') {
                                   

                                    products += '<div class="cart-btn"><a onclick="addToCart(this)" data-id="'+data.data[i]
                                        .product_id+'" data-type="simple" href="javascript:void(0)"><i class="flaticon-online-shop"></i>Add to Cart</a></div></div></div></div>';
                            

                                } else {
                                    products += '<div class="cart-btn"><a href="'+url+'/'+
                                    data
                                    .data[i].product_id + '/' + data
                                    .data[i].product_slug+'"><i class="flaticon-online-shop"></i>View Details</a></div></div></div></div>';
                            
                                }

                            
                        
                        }

                        $(".inline-block").html(products);
                        /*$('.three-column-carousel').owlCarousel({
			loop:true,
			margin:30,
			nav:false,
			smartSpeed: 3000,
			autoplay: true,
			navText: [ '<span class="flaticon-back"></span>', '<span class="flaticon-right"></span>' ],
			responsive:{
				0:{
					items:1
				},
				480:{
					items:1
				},
				600:{
					items:2
				},
				800:{
					items:2
				},
				1024:{
					items:3
				}
			}
		});
                        */

                        

                    }
                },
                error: function(data) {},
            });
        }

    });
    
  



  $("#contact-form").submit(function(e){
        e.preventDefault();
        $("#contact").attr('disabled',true);
        $("#contact").text('Submitting..');
        $('.invalid-feedback').css('display','none')
        first_name = $.trim($("#first_name").val());
        last_name = $.trim($("#address").val());
        email = $.trim($("#email").val());
        phone = $.trim($("#phone").val());
        message = $.trim($("#message").val());

        $.ajax({
        type: 'post',
        url: "{{ url('') }}" + '/api/client/contact-us',
        data:{
            first_name:first_name,
            last_name:last_name,
            email:email,
            phone:phone,
            message:message
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            clientid: "{{isset(getSetting()['client_id']) ? getSetting()['client_id'] : ''}}",
            clientsecret: "{{isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : ''}}",
        },
        beforeSend: function() {},
        success: function(data) {
            if (data.status == 'Success') {
                alert('Email sent successfully.');
                toastr.error('{{ trans("response.contact-form-success") }}');
                window.location.reload();
            }
            else{
                $("#contact").attr('disabled',false);
                $("#contact").text('Submit');
                toastr.error('{{ trans("response.some_thing_went_wrong") }}');
            }
        },
        error: function(data) {
            // console.log(data);
            if(data.status == 422){
                jQuery.each(data.responseJSON.errors, function(index, item) {
                    $("#"+index).parent().find('.invalid-feedback').css('display','block');
                    $("#"+index).parent().find('.invalid-feedback').html(item);
                });
            }
            else{
                toastr.error('{{ trans("response.some_thing_went_wrong") }}');;
            }
            $("#contact").attr('disabled',false);
             $("#contact").text('Submit');

        },
        });
    });
</script>




