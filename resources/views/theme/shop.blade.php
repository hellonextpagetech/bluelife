
@include('theme.cmn_head')
@section('content')

@endsection
<style>
  .no_prd{
    width:60%;
    margin:0 auto;
    display:block;
  }
  .shop-page-section{
        padding: 0px 0px 110px 0px;
  }
  .loading{
    margin:0 auto;
    display:block;
  }
  .text{
    text-align:left;
  }
  .single-shop-block .price{
        text-align:left;

}
.single-shop-block h3{
    text-align:left;
    
}
  .single-shop-block .image-box img {
    position: relative;
    display: inline-block;
    width: auto;
    height: 250px;
    transition: all 500ms ease;
}
.single-shop-block .image-box {
    position: relative;
    display: block;
    margin-top: 0;
}
.col-lg-4 {
    -ms-flex: 0 0 33.333333%;
    flex: 0 0 33.333333%;
    max-width: 32.333333%;
    display: inline-block;
}
.single-shop-block .text {
    position: relative;
    margin-bottom: 0px;
}

.loading1 h1{

margin:15px;
  }
  .prd-desc{
    padding:0px;
  }


.h-divider {
  margin: auto;
    margin-top: 40px;
    margin-bottom: 40px;
  position: relative;
}

.h-divider .shadow {
  overflow: hidden;
  height: 2px;
}

.h-divider .shadow:after {
  content: '';
  display: block;
  margin: -25px auto 0;
  width: 100%;
  height: 25px;
  border-radius: 125px/12px;
  box-shadow: 0 0 8px black;
}

.h-divider .text {
  width: 100px;
  height: 45px;
  padding: 10px;
  position: absolute;
  bottom: 100%;
  margin-bottom: -33px;
  left: 50%;
  margin-left: -60px;
  border-radius: 100%;
  box-shadow: 0 2px 4px #999;
  background: white;
}

.h-divider .text i {
  position: absolute;
  top: 4px;
  bottom: 4px;
  left: 4px;
  right: 4px;
  border-radius: 100%;
  border: 1px dashed #aaa;
  text-align: center;
  line-height: 50px;
  font-style: normal;
  color: #999;
}

.h-divider .text2 {
  width: 60px;
  height: 60px;
  position: absolute;
  bottom: 100%;
  margin-bottom: -35px;
  left: 50%;
  margin-left: -25px;
  border-radius: 100%;
  box-shadow: 0 2px 4px #999;
  background: white;
}

.h-divider img {
  margin: 5px auto;
    display: block;
    max-width: 30px;
    border-radius: 100%;
}
.separate:nth-child(even){
  background: #f1f1f182;
    padding: 10px 0;
}
  </style>
    <!--Page Title-->
    <section class="page-title centred" style="background-image: url({{ asset('theme/images/background/page-title.jpg')}});">
        <div class="auto-container">
           
        </div>
    </section>
    <!--End Page Title-->



  
  

    <!-- shop-page-section -->
    <section class="shop-page-section">
        <div class="auto-container">
            <div class="row clearfix">
                
                <div class="col-lg-12 col-md-12 col-sm-12 content-side">
                    <div class="our-shop">
                        <div class="row clearfix shop-pages">

                        </div>
                        <div class="section-title loading1">


                          @foreach($data['shopdata'] as $shop)
                          <div class="separate">
                            <h1 >{{$shop['category_name']}}</h1>
                                <div class="divider"></div>

                            @foreach($shop['products']  as $prd)

<div class="col-lg-4 col-md-6 col-sm-12 shop-block">
                            <div class="single-shop-block">
                              <div class="inner-box">
                                <div class="row">
                                  <div class="col-md-5">
                                      <a href="https://bluelife.co.in/public/product/12/variable-product-2"><figure class="image-box">
                                      <img src="https://bluelife.co.in/public///gallary/tu1.png" alt=""></figure>
                                    </a>
                                  </div>
                                  <div class="col-md-7 prd-desc">
<h3><a href="https://bluelife.co.in/public/product/12/variable-product-2">Tulips Ultra</a></h3>
                                  <div class="text">Unique water purifier with Digital RO technology and detachable stainless steel storage tank. Unique water purifier with..</div><div class="price">₹479.4 <del>₹940</del></div>
                                  
                                  </div>

                                </div>
                                
<div class="cart-btn">
                                    <a href="https://bluelife.co.in/public/product/12/variable-product-2"><i class="flaticon-online-shop"></i>View Details</a>
                                  </div>
                                

                                </div>
                              </div>


                            </div>
                            @endforeach
                            <div class="h-divider hide">
  <div class="shadow"></div>
  <div class="text2"><img src="{{ asset('theme/images/icons/bluelifefav.JPG')}}" /></div>
</div>
                          </div>
                          @endforeach


                        </div>
                        <div class="pagination-wrapper centred">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- shop-page-section end -->

   
@include('theme.cmn_footer')


<script>
  var language_id = localStorage.getItem('languageId');
  var attribute_id = [];
  var attribute = [];
  var variation_id = [];
  var variation =[];
  var sortBy = "";
  var sortType = "";
  var priceFromSidebar = "{{ isset($_GET['price']) ? $_GET['price']:'' }}";
  $(document).ready(function() {
    fetchProduct(1);
    
  });

 

  function fetchProduct(page){
    $('.load-more-products').attr('disabled',true);
    $('.load-more-products').html('Loading');
    $(".loading").html("<img src='https://thumbs.gfycat.com/FatherlyGoodAustrianpinscher-size_restricted.gif'/>");
    var limit = "{{ isset($_GET['limit']) ? $_GET['limit']:'120' }}";
    var category = "{{ isset($_GET['category']) ? $_GET['category'] :"" }}";
    var varations = "{{ isset($_GET['variation_id']) ? $_GET['variation_id'] :"" }}";
    var price_range = "{{ isset($_GET['price']) ? $_GET['price'] :"" }}";

    var url = "{{ url('') }}" +
            '/api/client/products?page='+page+'&limit='+limit+'&getCategory=1&getDetail=1&language_id=1&sortBy=id&sortType=DESC&currency=INR';
       
    if(category != "")
        url += "&productCategories="+category;   
    if(varations != "")
        url += "&variations="+varations;
    if(price_range != ""){
        price_range = price_range.split("-");
        url += "&price_from="+price_range[0];
        url += "&price_to="+price_range[1];
    }

    if(sortBy != "" && sortType != "")
        url += "&sortBy="+sortBy+"&sortType="+sortType;
    var searchinput = "{{ isset($_GET['search']) ? $_GET['search']:'' }}";
    if(searchinput != "")
      url +=  "&searchParameter="+searchinput;
    var appendTo = 'shop_page_product_card';
    $.ajax({
      type: 'get',
      url: url,
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        clientid: "{{isset(getSetting()['client_id']) ? getSetting()['client_id'] : ''}}",
        clientsecret: "{{isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : ''}}",
      },
      beforeSend: function() {},
      success: function(data) {
        console.log(data);
        if (data.status == 'Success') {
            const templ = document.getElementById("product-card-template");
            var products = "";
            
			 if(data.meta.last_page < page){
        $(".loading").html('');
              $('.load-more-products').attr('disabled',true);
              $('.load-more-products').html('No More Items');
            return;
          }
          var nextPage = parseInt(data.meta.current_page)+1;
            for (i = 0; i < data.data.length; i++) {
              if(data.data[i].product_id >5 && data.data[i].product_id <13){

                var length = 120;
                var url = "{{ URL('/product/')}}";
                var imgpath = "{{ asset('')}}"+'/'+data.data[i]
                            .product_gallary.detail[1].gallary_path;
                products += '<div class="col-lg-4 col-md-6 col-sm-12 shop-block"><div class="single-shop-block"><div class="inner-box"><div class="border-one"></div> <div class="border-two"></div><a href="'+url+'/'+
    data
    .data[i].product_id + '/' + data
    .data[i].product_slug+'"><figure class="image-box"><img src="'+imgpath+'" alt=""></figure></a>';
                products += '<h3><a href="'+url+'/'+
                        data
                        .data[i].product_id + '/' + data
                        .data[i].product_slug+'">'+data.data[i]
                        .detail[0].title+'</a></h3><div class="text">'+data.data[i]
                        .detail[0].desc.substring(0,length)+'..</div><div class="price">₹'+data.data[i]
                        .product_discount_price+' <del>₹'+data.data[i]
                        .product_price+'</del></div>';

                        if (data.data[i].product_type == 'simple') {
                             products += '<div class="cart-btn"><a onclick="addToCart(this)" data-id="'+data.data[i]
                                       .product_id+'" data-type="simple" href="javascript:void(0)"><i class="flaticon-online-shop"></i>Add to Cart</a></div></div></div></div>';
                               } else {
                                   products += '<div class="cart-btn"><a href="'+url+'/'+
                                   data
                                   .data[i].product_id + '/' + data
                                   .data[i].product_slug+'"><i class="flaticon-online-shop"></i>View Details</a></div></div></div></div>';
                           
                               }

                             }
           }
           $(".loading").html('');
           if(data.data.length>0){
            $(".shop-page").append(products);
            var pagination ='<button class="load-more-products btn btn-secondary" data-page="'+nextPage+'">Load More</button>';
            $(".pagination-wrapper").html(pagination);
           }else{
            var no_prd = "{{ asset('/images/no_product.png')}}";
            $(".shop-page").html('<img class="no_prd" src="'+no_prd+'" alt="">');
           }
            


        }
      },
      error: function(data) {},
    });
  }
  $(document).on('click','.load-more-products',function(){
    
    var pageToLoad = $(this).attr('data-page');
    fetchProduct(pageToLoad);
    
  })

</script>
