@include('theme.cmn_head')
@section('content')

@endsection
    <!--Page Title-->
    <section class="page-title centred" style="background-image: url({{ asset('theme/images/background/page-title.jpg')}});">
        <div class="auto-container">
            
        </div>
    </section>
    <!--End Page Title-->


    <!-- blog-grid -->
    <section class="blog-grid sec-pad">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-12 news-block">
                    <div class="news-block-one wow fadeInUp" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/resource/news-1.jpg')}}" alt=""></a></figure>
                            <div class="lower-content">
                                <div class="inner">
                                    <ul class="info-box clearfix">
                                        <li><a href="#"><i class="flaticon-user"></i>Emal Kanson</a></li>
                                        <li>Oct 25, 2019</li>
                                        <li><a href="#"><i class="flaticon-comment-white-oval-bubble"></i>Comments 34</a></li>
                                    </ul>
                                    <h2><a href="#">Producing Top Level Purified Bottled Water</a></h2>
                                    <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco Duis aute rue dolor prehendrit sit amet ...</div>
                                    <div class="lower-box clearfix">
                                        <div class="btn-box pull-left"><a href="#">Read More</a></div>
                                        <div class="share-box pull-right">
                                            <div class="share">
                                                <a href="#" class="share-link"><i class="fas fa-share-alt"></i>Share Post</a>
                                                <ul class="social-links">
                                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 news-block">
                    <div class="news-block-one wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/resource/news-2.jpg')}}" alt=""></a></figure>
                            <div class="lower-content">
                                <div class="inner">
                                    <ul class="info-box clearfix">
                                        <li><a href="#"><i class="flaticon-user"></i>Mahfuz Riad</a></li>
                                        <li>Oct 24, 2019</li>
                                        <li><a href="#"><i class="flaticon-comment-white-oval-bubble"></i>Comments 22</a></li>
                                    </ul>
                                    <h2><a href="#">How Water Useful For Our Body & Life</a></h2>
                                    <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco Duis aute rue dolor prehendrit sit amet ...</div>
                                    <div class="lower-box clearfix">
                                        <div class="btn-box pull-left"><a href="#">Read More</a></div>
                                        <div class="share-box pull-right">
                                            <div class="share">
                                                <a href="#" class="share-link"><i class="fas fa-share-alt"></i>Share Post</a>
                                                <ul class="social-links">
                                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 news-block">
                    <div class="news-block-one wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/resource/news-3.jpg')}}" alt=""></a></figure>
                            <div class="lower-content">
                                <div class="inner">
                                    <ul class="info-box clearfix">
                                        <li><a href="#"><i class="flaticon-user"></i>Mahfuz Riad</a></li>
                                        <li>Oct 23, 2019</li>
                                        <li><a href="#"><i class="flaticon-comment-white-oval-bubble"></i>Comments 20</a></li>
                                    </ul>
                                    <h2><a href="#">Ensuring With The Purest Best-Tasting Water</a></h2>
                                    <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco Duis aute rue dolor prehendrit sit amet ...</div>
                                    <div class="lower-box clearfix">
                                        <div class="btn-box pull-left"><a href="#">Read More</a></div>
                                        <div class="share-box pull-right">
                                            <div class="share">
                                                <a href="#" class="share-link"><i class="fas fa-share-alt"></i>Share Post</a>
                                                <ul class="social-links">
                                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 news-block">
                    <div class="news-block-one wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/resource/news-4.jpg')}}" alt=""></a></figure>
                            <div class="lower-content">
                                <div class="inner">
                                    <ul class="info-box clearfix">
                                        <li><a href="#"><i class="flaticon-user"></i>Mahfuz Riad</a></li>
                                        <li>Oct 22, 2019</li>
                                        <li><a href="#"><i class="flaticon-comment-white-oval-bubble"></i>Comments 30</a></li>
                                    </ul>
                                    <h2><a href="#">We Have World Class Quality At Low Cost Production </a></h2>
                                    <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco Duis aute rue dolor prehendrit sit amet ...</div>
                                    <div class="lower-box clearfix">
                                        <div class="btn-box pull-left"><a href="#">Read More</a></div>
                                        <div class="share-box pull-right">
                                            <div class="share">
                                                <a href="#" class="share-link"><i class="fas fa-share-alt"></i>Share Post</a>
                                                <ul class="social-links">
                                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 news-block">
                    <div class="news-block-one wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/resource/news-5.jpg')}}" alt=""></a></figure>
                            <div class="lower-content">
                                <div class="inner">
                                    <ul class="info-box clearfix">
                                        <li><a href="#"><i class="flaticon-user"></i>Mahfuz Riad</a></li>
                                        <li>Oct 21, 2019</li>
                                        <li><a href="#"><i class="flaticon-comment-white-oval-bubble"></i>Comments 10</a></li>
                                    </ul>
                                    <h2><a href="#">We Are Promised To Deliver Bottled Water In-Time</a></h2>
                                    <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco Duis aute rue dolor prehendrit sit amet ...</div>
                                    <div class="lower-box clearfix">
                                        <div class="btn-box pull-left"><a href="#">Read More</a></div>
                                        <div class="share-box pull-right">
                                            <div class="share">
                                                <a href="#" class="share-link"><i class="fas fa-share-alt"></i>Share Post</a>
                                                <ul class="social-links">
                                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 news-block">
                    <div class="news-block-one wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/resource/news-6.jpg')}}" alt=""></a></figure>
                            <div class="lower-content">
                                <div class="inner">
                                    <ul class="info-box clearfix">
                                        <li><a href="#"><i class="flaticon-user"></i>Mahfuz Riad</a></li>
                                        <li>Oct 20, 2019</li>
                                        <li><a href="#"><i class="flaticon-comment-white-oval-bubble"></i>Comments 15</a></li>
                                    </ul>
                                    <h2><a href="#">Equipped with Five Stage Purification System</a></h2>
                                    <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco Duis aute rue dolor prehendrit sit amet ...</div>
                                    <div class="lower-box clearfix">
                                        <div class="btn-box pull-left"><a href="#">Read More</a></div>
                                        <div class="share-box pull-right">
                                            <div class="share">
                                                <a href="#" class="share-link"><i class="fas fa-share-alt"></i>Share Post</a>
                                                <ul class="social-links">
                                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 news-block">
                    <div class="news-block-one wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/resource/news-7.jpg')}}" alt=""></a></figure>
                            <div class="lower-content">
                                <div class="inner">
                                    <ul class="info-box clearfix">
                                        <li><a href="#"><i class="flaticon-user"></i>Mahfuz Riad</a></li>
                                        <li>Oct 19, 2019</li>
                                        <li><a href="#"><i class="flaticon-comment-white-oval-bubble"></i>Comments 20</a></li>
                                    </ul>
                                    <h2><a href="#">How Water Useful For Our Body & Life</a></h2>
                                    <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco Duis aute rue dolor prehendrit sit amet ...</div>
                                    <div class="lower-box clearfix">
                                        <div class="btn-box pull-left"><a href="#">Read More</a></div>
                                        <div class="share-box pull-right">
                                            <div class="share">
                                                <a href="#" class="share-link"><i class="fas fa-share-alt"></i>Share Post</a>
                                                <ul class="social-links">
                                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 news-block">
                    <div class="news-block-one wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/resource/news-8.jpg')}}" alt=""></a></figure>
                            <div class="lower-content">
                                <div class="inner">
                                    <ul class="info-box clearfix">
                                        <li><a href="#"><i class="flaticon-user"></i>Mahfuz Riad</a></li>
                                        <li>Oct 18, 2019</li>
                                        <li><a href="#"><i class="flaticon-comment-white-oval-bubble"></i>Comments 25</a></li>
                                    </ul>
                                    <h2><a href="#">Produce Top Level Purified Bottled Water</a></h2>
                                    <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco Duis aute rue dolor prehendrit sit amet ...</div>
                                    <div class="lower-box clearfix">
                                        <div class="btn-box pull-left"><a href="#">Read More</a></div>
                                        <div class="share-box pull-right">
                                            <div class="share">
                                                <a href="#" class="share-link"><i class="fas fa-share-alt"></i>Share Post</a>
                                                <ul class="social-links">
                                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
                                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination-wrapper centred">
                <ul class="pagination clearfix">
                    <li><a href="#"><i class="flaticon-back"></i></a></li>
                    <li><a href="#" class="active">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#"><i class="flaticon-right"></i></a></li>
                </ul>
            </div>
        </div>
    </section>
    <!-- blog-grid end -->

    @include('theme.cmn_footer')
