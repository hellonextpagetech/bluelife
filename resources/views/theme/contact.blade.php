@include('theme.cmn_head')
@section('content')

@endsection
    <!--Page Title-->
    <section class="page-title centred" style="background-image: url({{ asset('theme/images/background/page-title.jpg')}});">
        <div class="auto-container">
            
        </div>
    </section>
    <!--End Page Title-->


    <!-- contact-section -->
    <section class="contact-section sec-pad">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-4 col-md-12 col-sm-12 info-column">
                    <div class="info-box">
                        <h2 class="title-text">Contact Information</h2>
                        <ul class="info-list clearfix wow fadeInUp" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <li><i class="fas fa-phone"></i><strong>Call Us</strong> <a href="tel:12078761059">+91 98490 67775</a></li>
                            <li><i class="fas fa-map-marker-alt"></i>BlueLife TechnoSciences India Pvt. Ltd, 
1-10-63&64, Chikoti Gardens,
Begumpet,Hyderabad - 016. <br />
Telangana, India.</li>
                            <li><i class="fas fa-envelope"></i><strong>E-mail</strong> <a href="mailto:sales@bluelife.com">sales@bluelife.com</a></li>
                            <li><i class="fas fa-clock"></i>Monday-Friday: 9am to 5pm<br />Saturday: 10am to 4pm<br />Sunday: Closed</li>
                        </ul>
                    </div>
                    <div class="info-box">
                        <h2 class="title-text">Operational Address</h2>
                        <ul class="info-list clearfix wow fadeInUp" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <li><i class="fas fa-map-marker-alt"></i>BlueLife TechnoSciences India Pvt. Ltd, 
1-10-63&64, Chikoti Gardens,
Begumpet,Hyderabad - 016. <br />
Telangana, India.</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 inner-column">
                    <div class="inner-box">
                        <h2 class="title-text">Send a Message</h2>

                        <div class="form-inner">
                            <form method="post"   id="contact-form" class="default-form">
                                @csrf
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                    <label>First Name</label>
                                        <input type="text" name="first_name" id="first_name" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                        <label>Last Name</label>
                                        <input type="text" name="last_name" id="last_name"  required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                        <label>Email</label>
                                        <input type="Email" name="email" id="contact_email" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                        <label>Phone</label>
                                        <input type="text" name="phone" id="contact_phone" required>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                        <label>Message</label>
                                        <textarea name="message" id="message" ></textarea>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group message-btn">
                                        <button type="submit" name="submit-form" id="contact">submit </button>
                                    </div>

                                </div>
                            </form>


                                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact-section end -->


    <!-- map-section -->
    <section class="map-section">
        <div class="google-map-area">
            <div >

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d487295.02532044525!2d78.12785129924684!3d17.41215307568293!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb99daeaebd2c7%3A0xae93b78392bafbc2!2sHyderabad%2C%20Telangana!5e0!3m2!1sen!2sin!4v1634023488292!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
    </section>
    <!-- map-section end -->
    @include('theme.cmn_footer')

    <script>

    $("#contact-form").submit(function(e){
        e.preventDefault();
        $("#contact").attr('disabled',true);
        $("#contact").text('Submitting..');
        $('.invalid-feedback').css('display','none')
        first_name = $.trim($("#first_name").val());
        last_name = $.trim($("#last_name").val());
        email = $.trim($("#contact_email").val());
        phone = $.trim($("#contact_phone").val());
        message = $.trim($("#message").val());

        $.ajax({
        type: 'post',
        url: "{{ url('') }}" + '/api/client/contact-us',
        data:{
            first_name:first_name,
            last_name:last_name,
            email:email,
            phone:phone,
            message:message
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            clientid: "{{isset(getSetting()['client_id']) ? getSetting()['client_id'] : ''}}",
            clientsecret: "{{isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : ''}}",
        },
        beforeSend: function() {},
        success: function(data) {
            if (data.status == 'Success') {
                alert('Email sent successfully.');
                toastr.error('{{ trans("response.contact-form-success") }}');
                window.location.reload();
            }
            else{
                $("#contact").attr('disabled',false);
                $("#contact").text('Submit');
                toastr.error('{{ trans("response.some_thing_went_wrong") }}');
            }
        },
        error: function(data) {
            // console.log(data);
            if(data.status == 422){
                jQuery.each(data.responseJSON.errors, function(index, item) {
                    $("#"+index).parent().find('.invalid-feedback').css('display','block');
                    $("#"+index).parent().find('.invalid-feedback').html(item);
                });
            }
            else{
                toastr.error('{{ trans("response.some_thing_went_wrong") }}');;
            }
            $("#contact").attr('disabled',false);
             $("#contact").text('Submit');

        },
        });
    });
</script>
