
    <!-- main-footer -->
    <footer class="main-footer">
        
        <div class="footer-upper footer-top">
            <div class="auto-container">
                <div class="widget-section wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="logo-widget footer-widget">
                                <figure class="footer-logo"><a href="index.php"><img src="{{ asset('theme/images/bluelife/logo-white.png')}}" alt=""></a></figure>
                                <div class="text">Our values are the key driving force those help us align the organisation towards customer sensitivity and deliver beyond customer's expectation.</div>
                            </div>
                            <div class="inner-box clearfix">
                    
                    <div class="footer-social pull-left">
                        <ul class="social-links clearfix"> 
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="links-widget footer-widget">
                                <h3 class="widget-title">Quick Links</h3>
                                <div class="widget-content">
                                    <ul class="list clearfix">
                                        <li><a href="{{ URL('page/about-us') }}">About Us</a></li>
                                        <li><a href="{{ URL('page/refund-policy') }}">Refund Policy</a></li>
                                        <li><a href="{{ URL('page/privacy-policy') }}">Privacy Policy</a></li>
                                        <li><a href="{{ URL('page/terms-and-conditions') }}">Terms & Conditions</a></li>
                                        <li><a href="{{ URL('contact-us') }}">Contact us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-4 col-md-6 col-sm-12 footer-column">
                            <div class="contact-widget footer-widget">
                                <h3 class="widget-title">Contact us</h3>
                                <div class="widget-content">
                                    <ul class="list clearfix">
                                        <li>BlueLife TechnoSciences India Pvt. Ltd, 
1-10-63&64, Chikoti Gardens,
Begumpet, <br />Hyderabad - 016.
Telangana, India.</li>
                                       <li>Call Us <a href="tel:9849067775">+91-98490 67775</a></li>
                                        <li>E-mail: <a href="mailto:info@bluelife.com">info@bluelife.com</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright" ><span style="float:left;">Copyrights &copy; 2021<a href="#"></a>. All rights reserved.</span> <span style="float:right;">Design & Developed by <a href="http://nextpagetechnologies.com/">Next Page Technologies Pvt Ltd</a></span></div>
            </div>
        </div>
    </footer>
    <!-- main-footer end -->



<!--Scroll to top-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fas fa-angle-up"></span>
</button>


<!-- jequery plugins -->
<script src="{{ asset('theme/js/jquery.js')}}"></script>
<script src="{{ asset('theme/js/popper.min.js')}}"></script>
<script src="{{ asset('theme/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('theme/js/owl.js')}}"></script>
<script src="{{ asset('theme/js/wow.js')}}"></script>
<script src="{{ asset('theme/js/validation.js')}}"></script>
<script src="{{ asset('theme/js/jquery.fancybox.js')}}"></script>
<script src="{{ asset('theme/js/scrollbar.js')}}"></script>

<script src="{{ asset('theme/js/jquery-ui.js')}}"></script> 
<script src="{{ asset('theme/js/appear.js')}}"></script>
<script src="{{ asset('theme/s/jquery.bootstrap-touchspin.js')}}"></script> 

<!-- main-js -->
<script src="{{ asset('theme/js/script.js')}}"></script>
<script src="{{ asset('theme/js/jquery.bootstrap-touchspin.js')}}"></script> 

<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    
<script>
  toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        loggedIn = $.trim(localStorage.getItem("customerLoggedin"));
        customerFname = $.trim(localStorage.getItem("customerFname"));
        customerLname = $.trim(localStorage.getItem("customerLname"));
        if (loggedIn != '1') {
            $(".auth-login").remove();
        } else {
            $(".without-auth-login").remove();
            $(".welcomeUsername").html(customerFname + " " + customerLname);
        }

        customerToken = $.trim(localStorage.getItem("customerToken"));


        languageId = localStorage.getItem("languageId");
        languageName = localStorage.getItem("languageName");

        if (languageName == null || languageName == 'null') {
            localStorage.setItem("languageId", $.trim("{{ $data['selectedLenguage'] }}"));
            localStorage.setItem("languageName", $.trim("{{ $data['selectedLenguageName'] }}"));
            $(".language-default-name").html($.trim("{{ $data['selectedLenguageName'] }}"));
            languageId = $.trim("{{ $data['selectedLenguage'] }}");
        } else {
            $(".language-default-name").html(localStorage.getItem("languageName"));
            $('.mobile-language option[value="' + localStorage.getItem("languageId") + '"]').attr('selected', 'selected');
        }

        currency = localStorage.getItem("currency");
        currencyCode = localStorage.getItem("currencyCode");
        if (currencyCode == null || currencyCode == 'null') {
            localStorage.setItem("currency", $.trim("{{ $data['selectedCurrency'] }}"));
            localStorage.setItem("currencyCode", $.trim("{{ $data['selectedCurrencyName'] }}"));
            $("#selected-currency").html($.trim("{{ $data['selectedCurrencyName'] }}"));
            currency = 1;
        } else {
            $("#selected-currency").html(localStorage.getItem("currencyCode"));
            $('.currency option[value="' + localStorage.getItem("languageId") + '"]').attr('selected', 'selected');
        }

        cartSession = $.trim(localStorage.getItem("cartSession"));
        if (cartSession == null || cartSession == 'null') {
            cartSession = '';
        }
        $(document).ready(function() {

            if (loggedIn != '1') {
                localStorage.setItem("cartSession", cartSession);
                menuCart(cartSession);
            } else {
                menuCart('');
            }

            //getWishlist();



        });

     function addToCart(input) {
            //$('.cart-btn').hide();

            $(input).text("Loading......");
            //$(input).parent().text("Loading......");
            product_type = $.trim($(input).attr('data-type'));
            product_id = $.trim($(input).attr('data-id'));
            product_combination_id = '';
            if (product_type == 'variable') {
                if ($.trim($("#product_combination_id").val()) == '' || $.trim($("#product_combination_id").val()) ==
                    'null') {
                    toastr.error("{{ trans('response.select-combination') }}")
                    return;
                }
                product_combination_id = $("#product_combination_id").val();
            }

            qty = $.trim($("#quantity-input").val());
            if (qty == '' || qty == 'undefined' || qty == null) {
                qty = 1;
            }
            addToCartFun(product_id, product_combination_id, cartSession, qty);
            setTimeout(function(){
                $(input).html('<i class="flaticon-online-shop"></i> Add to Cart');
            },500);
            //$(input).parent().text('');
        }

        function addToCartFun(product_id, product_combination_id, cartSession, qty) {
            if (loggedIn == '1') {
                url = "{{ url('') }}" + '/api/client/cart?session_id=' + cartSession + '&product_id=' + product_id +
                    '&qty=' + qty + '&product_combination_id=' + product_combination_id;
            } else {
                url = "{{ url('') }}" + '/api/client/cart/guest/store?session_id=' + cartSession + '&product_id=' +
                    product_id + '&qty=' + qty + '&product_combination_id=' + product_combination_id;
            }
            $.ajax({
                type: 'post',
                url: url,
                headers: {
                    'Authorization': 'Bearer ' + customerToken,
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
                    clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
                },
                beforeSend: function() {},
                success: function(data) {
                    if (data.status == 'Success') {
                        if (loggedIn != '1') {
                            localStorage.setItem("cartSession", data.data.session);
                            console.dir(data);
                            menuCart(data.data.session);
                        } else {
                            menuCart('');
                        }
                        alert('Product Added To Cart');
                        toastr.success('{{ trans('response.add-to-cart-success') }}')
                    } else if (data.status == 'Error') {

                        toastr.error('{{ trans('response.some_thing_went_wrong') }}');
                    }
                },
                error: function(data) {
                    console.log();
                    if (data.responseJSON.status == 'Error') {
                        // toastr.error(data.responseJSON.message);
                        toastr.error('{{ trans('response.some_thing_went_wrong') }}');
                    }

                },
            });
        }

        function menuCart(cartSession) {
            if (loggedIn == '1') {
                url = "{{ url('') }}" + '/api/client/cart?session_id=' + cartSession + '&currency=' + localStorage
                    .getItem("currency");
            } else {
                url = "{{ url('') }}" + '/api/client/cart/guest/get?session_id=' + cartSession + '&currency=' +
                    localStorage.getItem("currency");
            }
            $.ajax({
                type: 'get',
                url: url,
                headers: {
                    'Authorization': 'Bearer ' + customerToken,
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
                    clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
                },
                beforeSend: function() {},
                success: function(data) {
                    if (data.status == 'Success') {
                        $(".top-cart-product-show").html('');
                        
                        if (data.data.length > 0) {
                            
                            $(".cartcount").html(data.data.length);
                        } else {
                            $(".cartcount").html(0);
                        }
                    } else {
                        toastr.error('{{ trans('response.some_thing_went_wrong') }}');
                    }
                },
                error: function(data) {},
            });
        }


        $(document).on('click', '.quantity-plus', function() {
            var quantity = $('#quantity-input').val();
            $('#quantity-input').val(parseInt(quantity) + 1);
        })

        $(document).on('click', '.quantity-minus', function() {
            var quantity = $('#quantity-input').val();
            if (quantity > 1)
                $('#quantity-input').val(parseInt(quantity) - 1);
        });

        function removeCartItem(input) {

product_id = $.trim($(input).attr('data-id'));
product_combination_id = $.trim($(input).attr('data-combination-id'));
if (product_combination_id == null || product_combination_id == 'null') {
    product_combination_id = '';
}

if (loggedIn == '1') {
    url = "{{ url('') }}" + '/api/client/cart/delete?session_id=' + cartSession + '&product_id=' +
        product_id +
        '&product_combination_id=' + product_combination_id + '&language_id=' + languageId;
} else {
    url = "{{ url('') }}" + '/api/client/cart/guest/delete?session_id=' + cartSession + '&product_id=' +
        product_id + '&product_combination_id=' + product_combination_id + '&language_id=' + languageId;
}

$.ajax({
    type: 'DELETE',
    url: url,
    headers: {
        'Authorization': 'Bearer ' + customerToken,
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
        clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
    },
    beforeSend: function() {},
    success: function(data) {
        if (data.status == 'Success') {
            $(input).closest('tr').remove();
            cartItem(cartSession);
            menuCart(cartSession);
        } else {
            toastr.error('{{ trans('response.some_thing_went_wrong') }}');
        }
    },
    error: function(data) {},
});
}

/*$(".btn-secondary").on('click',function(){
    $(this).text("loading");

    setTimeout(function(){
                $(this).text("Continue");
            },1000);
});*/



        $('.log_out').click(function() {
            url = "{{ url('') }}" + '/api/client/customer_logout';

            $.ajax({
                type: 'post',
                url: url,
                headers: {
                    'Authorization': 'Bearer ' + customerToken,
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
                    clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
                },
                beforeSend: function() {},
                success: function(data) {
                    if (data.status == 'Success') {
                        localStorage.removeItem("customerToken");
                        localStorage.removeItem("customerHash");
                        localStorage.removeItem("customerLoggedin");
                        localStorage.removeItem("customerId");
                        localStorage.removeItem("customerFname");
                        localStorage.removeItem("customerLname");
                        localStorage.removeItem("cartSession", '');
                        location.reload();
                    }
                },
                error: function(data) {},
            });
        });



function gotoDealers(){
    var pincode = $('#srch_pincode').val();
    url = "{{ url('') }}" + '/srchdealers';
    $(".dealers_row").html('<div class="alert alert-success">Loadig...</div>');
    if(pincode.length<6){
    $(".dealers_row").html('<div class="alert alert-danger">Enter six digit pincode</div>');
    return;
    }
    $.ajax({
        method:'get',
        url:'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyA1ke3DWt4PrbFACoFVYYhdUGx1g2ycoqk&components=postal_code:'+pincode,
        success:function(data){
            if(data.status == 'OK'){
                var lat = data.results[0].geometry.location.lat;
                var lng = data.results[0].geometry.location.lng;
                $.ajax({
                    method:'post',
                    url:url,
                    data:{'pincode':pincode,'lat':lat,'lng':lng},
                    headers: {
            'Authorization': 'Bearer ' + customerToken,
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
            clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
        },
                    success:function(data){
                        console.log(data);
                        var resp = "";
                        if(data.length == 0){
                            $(".dealers_row").html('<div class="alert alert-danger">No dealers found for entered pincode.</div>');
                            return;
                        }
                        for (i = 0; i < data.length; i++) {
                            resp += '<div class="col-lg-4 col-md-6 col-sm-12 news-block"><div class="news-block-one wow fadeInUp" data-wow-delay="00ms" data-wow-duration="1500ms"><div class="inner-box"><div class="lower-content"><div class="inner"><ul class="info-box clearfix"><li><a href="#"><i class="flaticon-user"></i>'+data[i].org_name+'</a></li><br><li><a href="#"><i class="fas fa-phone"></i> '+data[i].mobile+'</a></li><li><a href="#"><i class="fas fa-envelope"></i> '+data[i].email+'</a></li><li><a href="#"><i class="fas fa-map"></i> '+data[i].distance.toFixed(2)+' Km</a></li></ul></div></div></div></div></div>';
                        }

                        $(".dealers_row").html(resp);
                        
                    }
                });
            }else{
                $(".dealers_row").html('<div class="alert alert-danger">No dealers found for entered pincode.</div>');
            }
        }
    });
}

function srchDealers() {
    url = "{{ url('') }}" + '/api/client/adddealers';

  //url = "{{ url('') }}" + '/srchdealers';

    $.ajax({
        type: 'post',
        url: url,
        data:{'org_name':'dfsdf'},
        headers: {
            'Authorization': 'Bearer ' + customerToken,
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
            clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
        },
        beforeSend: function() {},
        success: function(data) {
            if (data.status == 'Success') {
                
            }
        },
        error: function(data) {},
    }); 
        
}

function initAutocomplete() {

     var lat = '';
    var lng = '';
    var address = '500081';
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
         lat = results[0].geometry.location.lat();
         lng = results[0].geometry.location.lng();
        }
       else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
    alert('Latitude: ' + lat + ' Logitude: ' + lng);

  const map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -33.8688, lng: 151.2195 },
    zoom: 13,
    mapTypeId: "roadmap",
  });
  // Create the search box and link it to the UI element.
  const input = document.getElementById("pac-input");
  const searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  // Bias the SearchBox results towards current map's viewport.
  map.addListener("bounds_changed", () => {
    searchBox.setBounds(map.getBounds());
  });
  let markers = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener("places_changed", () => {
    const places = searchBox.getPlaces();
    console.log(places[0].geometry.location);
    jQuery("#latitude").val(places[0].geometry.location.lat());
    jQuery("#longitude").val(places[0].geometry.location.lng());
    if (places.length == 0) {
      return;
    }
    jQuery.ajax({
        method:'GET',
        url:'https://maps.googleapis.com/maps/api/geocode/json?latlng='+places[0].geometry.location.lat()+','+places[0].geometry.location.lng()+'&key=AIzaSyA1ke3DWt4PrbFACoFVYYhdUGx1g2ycoqk',
        success:function(response){
            console.log(response);
            var address = response.results[0].address_components;
            for(var i=0;i<address.length;i++){
                var pincode = address[i].long_name;
            }
            jQuery("#pincode").val(pincode);
            
        }
    });
    
    /*var geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(places[0].geometry.location.lat(), places[0].geometry.location.lng());
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
         if (status == google.maps.GeocoderStatus.OK) {
              if (results[0]) {
                var add = results[0].formatted_address ;
              }
         }
    }*/
    // Clear out the old markers.
    markers.forEach((marker) => {
      marker.setMap(null);
    });
    markers = [];
    // For each place, get the icon, name and location.
    const bounds = new google.maps.LatLngBounds();
    places.forEach((place) => {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }
      jQuery("#location-set").text(place.formatted_address);
      
      $.ajax({
            method:'POST',
            headers:{'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},
            url:'{{ URL("setlocation") }}',
            data:{"location":place.formatted_address},
            success:function(response){
            }
        });
      const icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25),
      };
      // Create a marker for each place.
      markers.push(
        new google.maps.Marker({
          map,
          icon,
          title: place.name,
          position: place.geometry.location,
        })
      );

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    
    map.fitBounds(bounds);
  });
}



$("#enquiry-form").submit(function(e){
        e.preventDefault();
        $("#enquiry-submit").attr('disabled',true);
        $("#enquiry-submit").text('Submitting..');
        $('.invalid-feedback').css('display','none')
        name = $.trim($("#name").val());
        phone = $.trim($("#phone").val());
        email = $.trim($("#email").val());
        city = $.trim($("#city").val());
        pincode = $.trim($("#pincode").val());
        request_for = $.trim($("#request_for").val());
        var error = 0;
        if(phone.length!=10){
            error = 1;
            $(".phone-error").html("Please enter valid 10 digit number");
        }else{
            $(".phone-error").html("");
        }
        if(pincode.length!=6){
            error = 1;
            $(".pincode-error").html("Please enter valid 6 digit pincode");
        }else{
            $(".pincode-error").html("");
        }
        if(error==1){
            $("#enquiry-submit").attr('disabled',false);
        $("#enquiry-submit").text('Submit');
            return false;
        }
        $.ajax({
        type: 'post',
        url: "{{ url('') }}" + '/api/client/enquiry',
        data:{
            name:name,
            phone:phone,
            email:email,
            city:city,
            pincode:pincode,
            request_for:request_for
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            clientid: "{{isset(getSetting()['client_id']) ? getSetting()['client_id'] : ''}}",
            clientsecret: "{{isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : ''}}",
        },
        beforeSend: function() {},
        success: function(data) {
            if (data.status == 'Success') {
                alert('Email sent successfully.');
                toastr.error('{{ trans("response.contact-form-success") }}');
                window.location.reload();
            }
            else{
                $("#enquiry-submit").attr('disabled',false);
                $("#enquiry-submit").text('Submit');
                toastr.error('{{ trans("response.some_thing_went_wrong") }}');
            }
        },
        error: function(data) {
            // console.log(data);
            if(data.status == 422){
                jQuery.each(data.responseJSON.errors, function(index, item) {
                    $("#"+index).parent().find('.invalid-feedback').css('display','block');
                    $("#"+index).parent().find('.invalid-feedback').html(item);
                });
            }
            else{
                toastr.error('{{ trans("response.some_thing_went_wrong") }}');;
            }
            $("#enquiry-submit").attr('disabled',false);
             $("#enquiry-submit").text('Submit');

        },
        });
    });



        </script>

</body><!-- End of .page_wrapper -->

</html>
