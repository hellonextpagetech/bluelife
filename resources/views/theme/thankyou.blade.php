
@include('theme.cmn_head')
@section('content')

@endsection
<style>
    .shop-page-section{
    padding: 0px 0px 110px 0px;
  }
  .pro-empty-page{
    margin: 0 auto;
  }
</style>
    <!--Page Title-->
    <section class="page-title centred" style="background-image: url({{ asset('theme/images/background/page-title.jpg')}});">
        <div class="auto-container">
            <div class="content-box">
            </div>
        </div>
    </section>
    <!--End Page Title-->


    <!-- shop-page-section -->
    <section class="shop-page-section">
        <div class="auto-container">
            <div class="row clearfix">
            <div class="pro-empty-page">
                        <h2 style="font-size: 150px;"><i class="far fa-check-circle"></i></h2>
                        <h1>{{  trans('lables.thank-you-title') }}</h1>
                        <p>
                            {{ trans('lables.thank-you-message') }} <a href="{{ url('/orders') }}" class="btn-link"><b>{{ 
                                trans('lables.thank-you-order-page') }}</b></a>.
                        </p>
                    </div>
             
            </div>
        </div>
    </section>
    <!-- shop-page-section end -->

   
@include('theme.cmn_footer')


