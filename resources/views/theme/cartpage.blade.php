
@include('theme.cmn_head')
@section('content')

@endsection
<style>
    body{
        font-family: inherit;

    }
    .shop-page-section {
    position: relative;
    padding: 0 0px 110px 0px;
}
</style>
    <!--Page Title-->
    <section class="page-title centred" style="background-image: url({{ asset('theme/images/background/page-title.jpg')}});">
        <div class="auto-container">
            
        </div>
    </section>
    <!--End Page Title-->


    <!-- shop-page-section -->
    <section class="shop-page-section">
        <div class="auto-container">
            <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 sideber-side">
                    <div class="shop-sidebar default-sidebar">
                        <div class="sidebar-post sidebar-widget">
                            <h3 class="widget-title">Shopping Cart</h3>
                            <div class="widget-content">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td>Product Name</td>
                                            <td>Image</td>
                                            <td>Quantity</td>
                                            <td>Price</td>
                                            <td>Action</td>
                                        </tr>
                                    <thead>
                                    <tbody class="cartitems">

                                    </tbody>
                                </table>
                                
                            </div>
                            <div class="sub-total clearfix">
                                <div class="price">Total: <span class="total">0.00</span></div>
                            </div>
                            <div class="col-md-3 btn-box" style="float:left;">
                                <a href="javascript:void(0)" onclick="updateCartItem()" class="cartupdate theme-btn style-one">Update Cart</a>
                            </div>
                            <div class="col-md-3 btn-box" style="float:right;">
                                <a href="{{URL('checkout')}}" class="theme-btn style-two">Checkout</a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!-- shop-page-section end -->

   
@include('theme.cmn_footer')


<script>
        languageId = $.trim(localStorage.getItem("languageId"));
        cartSession = $.trim(localStorage.getItem("cartSession"));
        if (cartSession == null || cartSession == 'null') {
            cartSession = '';
        }
        loggedIn = $.trim(localStorage.getItem("customerLoggedin"));
        customerToken = $.trim(localStorage.getItem("customerToken"));

        $(document).ready(function() {
            if (loggedIn == '1') {
                cartItem('');
                menuCart('');
            } else {
                cartItem(cartSession);
                menuCart(cartSession)
            }
        });

        


        function cartItem(cartSession) {
        if (loggedIn == '1') {
            url = "{{ url('') }}" + '/api/client/cart?session_id=' + cartSession + '&language_id=' + languageId+'&currency='+localStorage.getItem("currency");
        } else {
            url = "{{ url('') }}" + '/api/client/cart/guest/get?session_id=' + cartSession + '&language_id=' +
                languageId+'&currency='+localStorage.getItem("currency");
        }
        $.ajax({
            type: 'get',
            url: url,
            headers: {
                'Authorization': 'Bearer ' + customerToken,
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
                clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
            },
            beforeSend: function() {},
            success: function(data) {
                if (data.status == 'Success') {
                    console.log(data);
                    var cart = "";
                    total_price = 0;
                    if(data.data.length == 0){
                        window.location.href = "{{url('/')}}";
                    }
                    for (i = 0; i < data.data.length; i++) {
                        var url = "{{ URL('/product/')}}";
                        var d = '/\d/';
                var imgpath = "{{ asset('')}}"+'/'+data.data[i]
                            .product_gallary.detail[1].gallary_path;

                        cart += '<tr class="cartItem-row" product_combination_id="'+data
                                .data[i].product_combination_id+'" product_id="'+data
                                .data[i].product_id+'" product_type="'+data
                                .data[i].product_type+'"><td><a href="'+url+'/'+
                        data
                        .data[i].product_id + '/' + data
                        .data[i].product_slug+'">'+data.data[i]
                        .product_detail[0].title+'</a></td><td><div class="post"><figure class="image-box"><a href="'+url+'/'+
                        data
                        .data[i].product_id + '/' + data
                        .data[i].product_slug+'"><img src="'+imgpath+'" alt=""></a></figure>';
                        cart += '</div></td><td><div class="input-group item-quantity"><span class="input-group-btn"><button type="button" value="quantity'+i+'" class="quantity-left-minus btn cartItem-qty-2" data-type="minus" data-field="'+i+'"><span class="fas fa-minus"></span></button></span><input type="text" style="width:10px;" maxlength="2" value="'+data.data[i].qty+'" id="quantity'+i+'" name="quantity" class="form-control cartItem-qty"><span class="input-group-btn"><button type="button" value="quantity'+i+'" class="quantity-right-plus btn cartItem-qty-1" data-type="plus" data-field="'+i+'"><span class="fas fa-plus"></span></button></span> </div></td><td>₹'+data.data[i]
                        .discount_price+'</td><td><a href="javascript:void(0)" data-id=' + data.data[i]
                                        .product_id + ' data-combination-id=' + data
                                        .data[i].product_combination_id +
                                        ' onclick="removeCartItem(this)" class="cross-btn"><i class="fa fa-trash"</i></a></td></tr>';
                       total_price = total_price + (data.data[i].discount_price*data.data[i].qty);

                        
                    }

                   $('.cartitems').html(cart);
                   $('.total').html('₹'+total_price.toFixed(2));

                } else {
                    toastr.error('{{ trans("response.some_thing_went_wrong") }}');
                }
            },
            error: function(data) {},
        });
    }


        function couponCartItem() {
            coupon_code = $.trim($("#coupon_code").val());
            if (coupon_code == '') {
                toastr.error('{{ trans("coupon-code-required") }}');
                price = $(".caritem-subtotal").attr('price-symbol');
                $(".caritem-discount-coupon").html('');
                localStorage.setItem("couponCart", '');
                $(".caritem-grandtotal").html(price);
                return;
            }

            if($.trim($("#totalItems").val()) == '0'){
                toastr.error('{{ trans("cart-is-empty") }}');
                return;
            }

            $.ajax({
                type: 'post',
                url: "{{ url('') }}" + '/api/client/coupon?currency='+localStorage.getItem("currency"),
                data: {
                    coupon_code: coupon_code,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Authorization': 'Bearer ' + customerToken,
                },
                beforeSend: function() {},
                success: function(data) {
                    $("#coupon_code").val(coupon_code);
                    if (data.status == 'Success') {
                        if (data.data.type == 'fixed') {
                            price = $(".caritem-subtotal").attr('price');
                            if(data.data.currency != '' && data.data.currency != 'null' && data.data.currency != null){
                                price1 = (price - (data.data.amount * data.data.currency.exchange_rate ));
                                if(data.data.currency.symbol_position == 'left'){
                                    $(".caritem-discount-coupon").html(data.data.currency.code +' '+ data.data.amount);
                                    $(".caritem-grandtotal").html(data.data.currency.code +' '+ price1.toFixed(2));
                                }
                                else{
                                    $(".caritem-discount-coupon").html(data.data.amount +' '+ data.data.currency.code);
                                    $(".caritem-grandtotal").html(price1.toFixed(2) +' '+ data.data.currency.code);
                                }
                            }
                        } else {
                            if(data.data.currency != '' && data.data.currency != 'null' && data.data.currency != null){
                                if(data.data.currency.symbol_position == 'left'){
                                    price = $(".caritem-subtotal").attr('price');
                                    price1 = (price / 100) * data.data.amount;
                                    $(".caritem-discount-coupon").html(data.data.currency.code +' '+ price1.toFixed(2));
                                    price = price - price1
                                    $(".caritem-grandtotal").html(data.data.currency.code +' '+ price.toFixed(2));
                                }
                                else{
                                    price = $(".caritem-subtotal").attr('price');
                                    price1 = (price / 100) * data.data.amount;
                                    $(".caritem-discount-coupon").html(price1.toFixed(2) +' '+ data.data.currency.code);
                                    price = price - price1
                                    $(".caritem-grandtotal").html(price.toFixed(2) +' '+ data.data.currency.code);
                                }
                            }
                        }
                        localStorage.setItem("couponCart", coupon_code);
                    } else {
                        price = $(".caritem-subtotal").attr('price-symbol');
                        $(".caritem-discount-coupon").html('');
                        $(".caritem-grandtotal").html(price);
                        localStorage.setItem("couponCart", '');
                        toastr.error('{{ trans("invalid-coupon") }}');
                    }
                },
                error: function(data) {
                    console.log(data);
                    price = $(".caritem-subtotal").attr('price-symbol');
                    $(".caritem-discount-coupon").html('');
                    $(".caritem-grandtotal").html(price);
                    localStorage.setItem("couponCart", '');
                    if (data.status == 422) {
                        // toastr.error(data.res
                        toastr.error('{{ trans("response.some_thing_went_wrong") }}');
                    }
                },
            });
        }


        function updateCartItem() {
            $(".cartupdate").text('Updating...');
            len = $(".cartItem-row").length;
            for (i = 0; i < len; i++) {
                product_id = $(".cartItem-row").eq(i).attr('product_id');
                qty = $(".cartItem-row").eq(i).find('.cartItem-qty').val();

                product_type = $(".cartItem-row").eq(i).attr('product_type');
                product_combination_id = '';
                if (product_type == 'variable') {
                    if ($.trim($(".cartItem-row").eq(i).attr('product_combination_id')) == '' || $.trim($(".cartItem-row")
                            .eq(i).attr('product_combination_id')) == 'null') {
                        toastr.error('{{ trans("combination-missing") }}');
                        return;
                    }
                    product_combination_id = $(".cartItem-row").eq(i).attr('product_combination_id');
                }

                addToCartFun(product_id, product_combination_id, cartSession, qty);
            }

            cartItem(cartSession);
            couponCart = $.trim(localStorage.getItem("couponCart"));
            if (couponCart != 'null' && couponCart != '') {
                $("#coupon_code").val(couponCart);
                couponCartItem();
            }
            setTimeout(function(){
                $(".cartupdate").text('UPDATE CART');
                window.location.reload();
            },1000);
           // $(".cartupdate").text('UPDATE CART');
        }


        $(document).on('click', '.quantity-right-plus', function() {
            var row_id = $(this).attr('data-field');

            var quantity = $('#quantity' + row_id).val();
            $('#quantity' + row_id).val(parseInt(quantity) + 1);
        });

        $(document).on('click', '.quantity-left-minus', function() {
            var row_id = $(this).attr('data-field');
            var quantity = $('#quantity' + row_id).val();
            if (quantity > 1)
                $('#quantity' + row_id).val(parseInt(quantity) - 1);
        });


        
    </script>
