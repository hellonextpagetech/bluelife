@include('theme.cmn_head')
@section('content')

@endsection
    <!--Page Title-->
    <section class="page-title centred" style="background-image: url({{ asset('theme/images/background/page-title.jpg')}});">
        <div class="auto-container">
            <div class="content-box">
                <h1>Shop Details</h1>
                <div class="text">Pure Quality Drinking Water</div>
            </div>
        </div>
    </section>
    <!--End Page Title-->


    <!-- shop-details -->
    <section class="shop-details">
        <div class="product-details-content">
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                        <figure class="image-box"><a href="images/bluelife/TulipsULTRA/TU.png" class="lightbox-image"><img src="images/bluelife/TulipsULTRA/TU.png" alt=""></a></figure>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                        <div class="content-box">
                            <h2 class="prd-title">Tulips Ultra</h2>
                            <div class="text prd-half-desc">Exercitation lamco laboris aliquip duis aute irure dolor in reprehenderid voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa.</div>
                            <div class="price">₹17.00 <del>₹29.50</del></div>
                            
                            <div class="addto-cart-box clearfix">
                                <div class="item-quantity">
                                    <input class="quantity-spinner" type="text" value="1" name="quantity">
                                </div>
                                <div class="cart-btn"><button type="button"><i class="flaticon-online-shop"></i>buy now</button></div>
                            </div>
                            <ul class="categories list-item">
                                <li>Categories:</li>
                                <li class="category"><a href="#">Natural Water</a>,</li>
                            </ul>
                            <ul class="categories list-item">
                                <li>Tags:</li>
                                <li><a href="#">Natural Water</a>,</li>
                                <li><a href="#">Bottle</a></li>
                            </ul>
                            <ul class="product-ids list-item">
                                <li>Product ID:</li>
                                <li class="product-id">3638</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-info-tabs">
            <div class="border-shap">
                <div class="border-3" style="background-image: url(images/icons/border-3.png);"></div>
            </div>
            <div class="auto-container">
                <div class="product-tab tabs-box">
                    <ul class="tab-btns tab-buttons centred clearfix">
                        <li class="tab-btn active-btn" data-tab="#tab-1">Description</li>
                        <li class="tab-btn" data-tab="#tab-2">Additional information</li>
                        <li class="tab-btn" data-tab="#tab-3">Reviews (0)</li>
                    </ul>
                    <div class="tabs-content">
                        <div class="tab active-tab clearfix" id="tab-1">
                            <div class="text prd-desc">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>
                                <p>Anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                            </div>
                        </div>
                        <div class="tab clearfix" id="tab-2">
                            <div class="text">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>
                                <p>Anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                            </div>
                        </div> 
                        <div class="tab clearfix" id="tab-3">
                            <div class="text reviews">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>
                                <p>Anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                            </div>
                        </div>              
                    </div>
                </div>
            </div>
        </div>
        <div class="related-product">
            <div class="auto-container">
                <div class="top-title clearfix">
                    <div class="title-inner">
                        <div class="sec-title"><h1>Related Bottles</h1></div>
                    </div>
                    <div class="text-inner">
                        <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco exa consequat. Duis aute rue dolor prehendrit lorem ipsum sit amet consectetur adipisicing sed.</div>
                    </div>
                </div>
                <div class="row clearfix related-page">
                    
                <div class="col-lg-4 col-md-6 col-sm-12 shop-block">
                                <div class="single-shop-block">
                                    <div class="inner-box">
                                        <div class="border-one"></div>
                                        <div class="border-two"></div>
                                        <figure class="image-box"><img src="images/bluelife/TulipsULTRA/tu1.png" alt=""></figure>
                                        <h3><a href="shop-details.php">Tuplis Ultra</a></h3>
                                        <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                                        <div class="price">₹17.00 <del>₹29.50</del></div>
                                        <div class="cart-btn"><a href="shop-details.php"><i class="flaticon-online-shop"></i>Add to Cart</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 shop-block">
                                <div class="single-shop-block">
                                    <div class="inner-box">
                                        <div class="border-one"></div>
                                        <div class="border-two"></div>
                                        <figure class="image-box"><img src="images/bluelife/Emerald/Emerald-P1.png" alt=""></figure>
                                        <h3><a href="shop-details.php">Emerald</a></h3>
                                        <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                                        <div class="price">₹24.00 <del>₹35.50</del></div>
                                        <div class="cart-btn"><a href="shop-details.php"><i class="flaticon-online-shop"></i>Add to Cart</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12 shop-block">
                                <div class="single-shop-block">
                                    <div class="inner-box">
                                        <div class="border-one"></div>
                                        <div class="border-two"></div>
                                        <figure class="image-box"><img src="images/bluelife/Aarwa/aarwa2.png" alt=""></figure>
                                        <h3><a href="shop-details.php">Aarwa</a></h3>
                                        <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                                        <div class="price">₹39.00 <del>₹55.50</del></div>
                                        <div class="cart-btn"><a href="shop-details.php"><i class="flaticon-online-shop"></i>Add to Cart</a></div>
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>
    </section>
    <!-- shop-details end -->

    <footer class="main-footer">
        <div class="footer-top">
            <div class="border-shap">
                <div class="border-3" style="background-image: url(images/icons/border-4.png);"></div>
            </div>
            <div class="auto-container">
                <div class="inner-box clearfix">
                    <div class="subscribe-form pull-left">
                        <form action="#" method="post">
                            <div class="form-group">
                                <input type="email" name="email" placeholder="Email address to subscribe" required="">
                                <button type="submit" class="theme-btn style-two">subscribe</button>
                            </div>
                        </form>
                    </div>
                    <div class="footer-social pull-right">
                        <ul class="social-links clearfix"> 
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                            <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-upper">
            <div class="auto-container">
                <div class="widget-section wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="logo-widget footer-widget">
                                <figure class="footer-logo"><a href="index.php"><img src="images/bluelife/logo-white.png" alt=""></a></figure>
                                <div class="text">Aliquaut enim mini veniam quis trud exerc tation ullamco exa consequat. Duis aute rue dolor prehendrit lorem ipsum sit amet cons ctetur adipisicing sed.</div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="links-widget footer-widget">
                                <h3 class="widget-title">About Us</h3>
                                <div class="widget-content">
                                    <ul class="list clearfix">
                                        <li><a href="#">Why Choose Us</a></li>
                                        <li><a href="#">Free Water Bottles</a></li>
                                        <li><a href="#">Water Dispensers</a></li>
                                        <li><a href="#">Bottled Water Coolers</a></li>
                                        <li><a href="#">Contact us</a></li>
                                        <li><a href="#">Terms & Conditions</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="shediul-widget footer-widget">
                                <h3 class="widget-title">Business Hours</h3>
                                <div class="widget-content">
                                    <ul class="list clearfix">
                                        <li>Monday-Friday: 9am to 5pm</li>
                                        <li>Saturday: 10am to 4pm</li>
                                        <li>Sunday: Closed</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="contact-widget footer-widget">
                                <h3 class="widget-title">Contact us</h3>
                                <div class="widget-content">
                                    <ul class="list clearfix">
                                        <li>102,Sai Nilayam, Cherlapalli, Hyderabad</li>
                                        <li>Call Us <a href="tel:9999999999">+1 9999-999-999</a></li>
                                        <li>E-mail: <a href="mailto:info@bluelife.com">info@bluelife.com</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright">Copyrights &copy; 2019 <a href="#"></a>. All rights reserved.</div>
            </div>
        </div>
    </footer>



<!--Scroll to top-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fas fa-angle-up"></span>
</button>


<!-- jequery plugins -->
<script src="{{ asset('theme/js/jquery.js')}}"></script>
<script src="{{ asset('theme/js/popper.min.js')}}"></script>
<script src="{{ asset('theme/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('theme/js/owl.js')}}"></script>
<script src="{{ asset('theme/js/wow.js')}}"></script>
<script src="{{ asset('theme/js/validation.js')}}"></script>
<script src="{{ asset('theme/js/jquery.fancybox.js')}}"></script>
<script src="{{ asset('theme/js/scrollbar.js')}}"></script>
<script src="{{ asset('theme/js/jquery-ui.js')}}"></script> 
<script src="{{ asset('theme/js/appear.js')}}"></script>
<script src="{{ asset('theme/js/jquery.bootstrap-touchspin.js')}}"></script> 

<!-- map script -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyATY4Rxc8jNvDpsK8ZetC7JyN4PFVYGCGM"></script>
<script src="{{ asset('theme/js/gmaps.js')}}"></script>
<script src="{{ asset('theme/js/map-helper.js')}}"></script>

<!-- main-js -->
<script src="{{ asset('theme/js/script.js')}}"></script>

</body><!-- End of .page_wrapper -->

</html>


<script>
    var attribute_id = [];
    var attribute = [];
    var variation_id = [];
    var variation = [];
    $(document).ready(function() {
        fetchProduct();
        fetchRelatedProduct();
    });

    languageId = localStorage.getItem("languageId");
    if (languageId == null || languageId == 'null') {
        localStorage.setItem("languageId", '1');
        $(".language-default-name").html('Endlish');
        localStorage.setItem("languageName", 'English');
        languageId = 1;
    }

    customerToken = $.trim(localStorage.getItem("customerToken"));


    function fetchProduct() {
        var url = "{{ url('') }}" + '/api/client/products/' + "{{ $product }}" +
            '?getCategory=1&getDetail=1&language_id=' + languageId + '&currency=INR';
        var appendTo = 'product-page';
        $.ajax({
            type: 'get',
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
                clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
            },
            beforeSend: function() {},
            success: function(data) {
                if (data.status == 'Success') {
                    console.log(data);
                    var length = 120;
                    var imgpath = "{{ asset('')}}"+'/'+data.data
                            .product_gallary.detail[1].gallary_path;
                   $(".prd-title").html(data.data.detail[0].title);

                   $(".prd-half-desc").html(data.data.detail[0].desc.substring(0,length));
                   $(".prd-desc").html(data.data.detail[0].desc);

                   $(".product-id").html(data.data.detail[0].product_id);


                   $(".category").html(data.data.category[0].category_detail.detail[0].name);


                   $(".price").html("₹"+data.data
                        .product_discount_price+"<del>"+"₹"+data.data
                        .product_price);
                    
                    if(data.data.reviews.length==0){
                        $(".reviews").html('No reviews');
                    }

                   var img = '<figure class="image-box"><a href="'+imgpath+'" class="lightbox-image"><img src="'+imgpath+'" alt=""></a></figure>';
                   $(".image-column").html(img);

                }   
            },
            error: function(data) {},
        });
    }

   
    function fetchRelatedProduct() {
        var url = "{{ url('') }}" + '/api/client/products?limit=10&getDetail=1&language_id=' + languageId + '&currency='+localStorage.getItem("currency");
        var appendTo = 'related';
        $.ajax({
            type: 'get',
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
                clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
            },
            beforeSend: function() {},
            success: function(data) {
                if (data.status == 'Success') {
                    var products = "";

for (i = 0; i < data.data.length; i++) {
    var length = 120;
var url = "{{ URL('/product/')}}";
var imgpath = "{{ asset('')}}"+'/'+data.data[i]
        .product_gallary.detail[1].gallary_path;
products += '<div class="col-lg-4 col-md-6 col-sm-12 shop-block"><div class="single-shop-block"><div class="inner-box"><div class="border-one"></div> <div class="border-two"></div><a href="'+url+'/'+
    data
    .data[i].product_id + '/' + data
    .data[i].product_slug+'"><figure class="image-box"><img src="'+imgpath+'" alt=""></figure></a>';
products += '<h3><a href="'+url+'/'+
    data
    .data[i].product_id + '/' + data
    .data[i].product_slug+'">'+data.data[i]
    .detail[0].title+'</a></h3><div class="text">'+data.data[i]
    .detail[0].desc.substring(0,length)+'..</div><div class="price">₹'+data.data[i]
    .product_discount_price+' <del>₹'+data.data[i]
    .product_price+'</del></div>';
products += '<div class="cart-btn"><a href="'+url+'/'+
    data
    .data[i].product_id + '/' + data
    .data[i].product_slug+'"><i class="flaticon-online-shop"></i>Add to Cart</a></div></div></div></div>';


}
$(".related-page").html(products);
                }
            },
            error: function(data) {},
        });
    }

    function productReview() {
        rating = $("input[name=rating]").val();
        comment = $("#comment").val();
        title = $("#title").val();
        if(rating == ''){
            toastr.error('{{ trans("select-ratings") }}');
            return;
        }

        var url = "{{ url('') }}" + '/api/client/review?product_id={{ $product }}&comment=' + comment + '&rating=' + rating +'&title='+title;
        var appendTo = 'related';
        $.ajax({
            type: 'post',
            url: url,
            headers: {
                'Authorization': 'Bearer ' + customerToken,
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
                clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
            },
            beforeSend: function() {},
            success: function(data) {
                if (data.status == 'Success') {
                    toastr.success('{{ trans("rating-saved-successfully") }}');
                    $("#comment").val('');
                    $("#title").val('');
                    getProductReview();
                }
            },
            error: function(data) {
                console.log(data);
                if (data.status == 422) {
                    jQuery.each(data.responseJSON.errors, function(index, item) {
                        $("#" + index).parent().find('.invalid-feedback').css('display',
                            'block');
                        $("#" + index).parent().find('.invalid-feedback').html(item);
                    });
                }
                else if (data.status == 401) {
                    toastr.error('{{ trans("response.some_thing_went_wrong") }}');
                }
            },
        });
    }

    function getProductReview() {
        var url = "{{ url('') }}" + '/api/client/review?product_id={{ $product }}';
        $.ajax({
            type: 'get',
            url: url,
            headers: {
                'Authorization': 'Bearer ' + customerToken,
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
                clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
            },
            beforeSend: function() {},
            success: function(data) {
                if (data.status == 'Success') {
                    const temp2 = document.getElementById("review-rating-template");
                    $("#review-rating-show").html('');
                    for (review = 0; review < data.data.length; review++) {
                        const clone1 = temp2.content.cloneNode(true);
                        clone1.querySelector(".review-comment").innerHTML = data.data[review].comment;
                        clone1.querySelector(".review-date").innerHTML = data.data[review].date;
                        clone1.querySelector(".review-title").innerHTML = data.data[review].title;
                        if (data.data[review].rating == '5') {
                            clone1.querySelector(".review-rating5").setAttribute('checked', true);
                        } else if (data.data[review].rating == '4') {
                            clone1.querySelector(".review-rating4").setAttribute('checked', true);
                        } else if (data.data[review].rating == '3') {
                            clone1.querySelector(".review-rating3").setAttribute('checked', true);
                        } else if (data.data[review].rating == '2') {
                            clone1.querySelector(".review-rating2").setAttribute('checked', true);
                        } else if (data.data[review].rating == '1') {
                            clone1.querySelector(".review-rating1").setAttribute('checked', true);
                        }
                        $("#review-rating-show").append(clone1);
                    }
                }
            },
            error: function(data) {
                console.log(data);
            },
        });
    }


    function slideInital() {
        // Product SLICK
        // $('.slider-show').html('<div class="slider-for"></div><div class="slider-nav"></div>');
        // alert();
        jQuery('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            infinite: false,
            draggable: false,
            fade: true,
            asNavFor: '.slider-nav',
            reinit : true
        });
        jQuery('.slider-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            centerMode: true,
            centerPadding: '60px',
            dots: false,
            arrows: true,
            focusOnSelect: true,
            reinit : true
        });


        // Product vertical SLICK
        jQuery('.slider-for-vertical').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            infinite: false,
            draggable: false,
            fade: true,
            asNavFor: '.slider-nav-vertical'
        });
        jQuery('.slider-nav-vertical').slick({
            dots: false,
            arrows: true,
            vertical: true,
            asNavFor: '.slider-for-vertical',
            slidesToShow: 3,
            // centerMode: true,
            slidesToScroll: 1,
            verticalSwiping: true,
            focusOnSelect: true
        });

        jQuery(function() {
            // ZOOM
            jQuery('.ex1').zoom();

        });

    }
</script>


