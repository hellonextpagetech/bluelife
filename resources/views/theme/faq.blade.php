@include('theme.cmn_head')
@section('content')

@endsection
    <!--Page Title-->
    <section class="page-title centred" style="background-image: url({{ asset('theme/images/background/page-title.jpg')}});">
        <div class="auto-container">
            
        </div>
    </section>
    <!--End Page Title-->


    <!-- faq-section -->
    <section class="faq-section">
        <div class="auto-container">
            <h2 class="faq-title">Need more info? Get your answers...</h2>
            <div class="row clearfix">
                <div class="col-lg-6 col-md-12 col-sm-12 inner-column">
                    <div class="inner-box">
                        <div class="faq-content">
                            <ul class="accordion-box active-block">
                                <li class="accordion block">
                                    <div class="acc-btn">
                                        <div class="icon-outer"><i class="fas fa-angle-right"></i></div>
                                        <h3>Why Does Water Filteration Work?</h3>
                                    </div>
                                    <div class="acc-content">
                                        <div class="content">
                                            <div class="text">Esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat sed cup datat non proident, sunt in culpa qui officia deserunt mollit anim idpsum est laborum. Sed uet perspiciatis unde omnis iste.</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="accordion block">
                                    <div class="acc-btn active">
                                        <div class="icon-outer"><i class="fas fa-angle-right"></i></div>
                                        <h3>Mineral Water vs Spring Water. Difference?</h3>
                                    </div>
                                    <div class="acc-content current">
                                        <div class="content">
                                            <div class="text">Esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat sed cup datat non proident, sunt in culpa qui officia deserunt mollit anim idpsum est laborum. Sed uet perspiciatis unde omnis iste.</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="accordion block">
                                     <div class="acc-btn">
                                        <div class="icon-outer"><i class="fas fa-angle-right"></i></div>
                                        <h3>How Mineral Water Treated Chemically?</h3>
                                    </div>
                                    <div class="acc-content">
                                        <div class="content">
                                            <div class="text">Esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat sed cup datat non proident, sunt in culpa qui officia deserunt mollit anim idpsum est laborum. Sed uet perspiciatis unde omnis iste.</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="accordion block">
                                     <div class="acc-btn">
                                        <div class="icon-outer"><i class="fas fa-angle-right"></i></div>
                                        <h3>What Is Natural Mineral Water SB?</h3>
                                    </div>
                                    <div class="acc-content">
                                        <div class="content">
                                            <div class="text">Esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat sed cup datat non proident, sunt in culpa qui officia deserunt mollit anim idpsum est laborum. Sed uet perspiciatis unde omnis iste.</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="accordion block">
                                     <div class="acc-btn">
                                        <div class="icon-outer"><i class="fas fa-angle-right"></i></div>
                                        <h3>Can Baby Drink Uaques Distilled Water?</h3>
                                    </div>
                                    <div class="acc-content">
                                        <div class="content">
                                            <div class="text">Esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat sed cup datat non proident, sunt in culpa qui officia deserunt mollit anim idpsum est laborum. Sed uet perspiciatis unde omnis iste.</div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 inner-column">
                    <div class="inner-box">
                        <div class="faq-content">
                            <ul class="accordion-box active-block">
                                <li class="accordion block">
                                    <div class="acc-btn">
                                        <div class="icon-outer"><i class="fas fa-angle-right"></i></div>
                                        <h3>Does Mineral Water Contains Chlorine ?</h3>
                                    </div>
                                    <div class="acc-content">
                                        <div class="content">
                                            <div class="text">Esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat sed cup datat non proident, sunt in culpa qui officia deserunt mollit anim idpsum est laborum. Sed uet perspiciatis unde omnis iste.</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="accordion block">
                                    <div class="acc-btn">
                                        <div class="icon-outer"><i class="fas fa-angle-right"></i></div>
                                        <h3>Where Mineral Water Sourced From?</h3>
                                    </div>
                                    <div class="acc-content">
                                        <div class="content">
                                            <div class="text">Esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat sed cup datat non proident, sunt in culpa qui officia deserunt mollit anim idpsum est laborum. Sed uet perspiciatis unde omnis iste.</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="accordion block">
                                     <div class="acc-btn">
                                        <div class="icon-outer"><i class="fas fa-angle-right"></i></div>
                                        <h3>How Mineral Water Treated Chemically?</h3>
                                    </div>
                                    <div class="acc-content">
                                        <div class="content">
                                            <div class="text">Esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat sed cup datat non proident, sunt in culpa qui officia deserunt mollit anim idpsum est laborum. Sed uet perspiciatis unde omnis iste.</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="accordion block">
                                     <div class="acc-btn">
                                        <div class="icon-outer"><i class="fas fa-angle-right"></i></div>
                                        <h3>How Is Sparkling Water Made?</h3>
                                    </div>
                                    <div class="acc-content">
                                        <div class="content">
                                            <div class="text">Esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat sed cup datat non proident, sunt in culpa qui officia deserunt mollit anim idpsum est laborum. Sed uet perspiciatis unde omnis iste.</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="accordion block">
                                     <div class="acc-btn active">
                                        <div class="icon-outer"><i class="fas fa-angle-right"></i></div>
                                        <h3>I’ve Bought Water But It Is Not Very Real?</h3>
                                    </div>
                                    <div class="acc-content current">
                                        <div class="content">
                                            <div class="text">Esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat sed cup datat non proident, sunt in culpa qui officia deserunt mollit anim idpsum est laborum. Sed uet perspiciatis unde omnis iste.</div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </section>
    <!-- faq-section end -->

    @include('theme.cmn_footer')
