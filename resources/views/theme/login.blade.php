
@include('theme.cmn_head')
@section('content')

@endsection
    <!--Page Title-->
    <section class="page-title centred" style="background-image: url({{ asset('theme/images/background/page-title.jpg')}});">
        <div class="auto-container">
            <div class="content-box">
            </div>
        </div>
    </section>
    <!--End Page Title-->


    <!-- shop-page-section -->
    <section class="shop-page-section">
        <div class="auto-container">
            <div class="row clearfix">
            <div class="col-12 col-sm-12 col-md-6">

                <div class="heading">
                    <h2>
                        {{ trans('lables.login-login') }}
                    </h2>
                    <hr>
                </div>
                <div class="col-12 registration-process">

                    <form id="loginForm">
                        <div class="row">
                            <div class="from-group mb-3 col-12">
                                <label for="inlineFormInputGroup">Mobile</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="loginEmail" placeholder="Enter mobile number">
                                </div>
                                <small class="loginemail errors d-none" style="color:red"></small>
                            </div>
                        </div>

                        <div class="row d-none otp-login-row">
                            <div class="from-group mb-3 col-12">
                                <label for="inlineFormInputGroup">OTP</label>
                                <div class="input-group">

                                    <input type="text" class="form-control" id="loginotp" placeholder="Enter otp sent to your mobile number">
                                </div>
                                <small class="loginotp errors d-none" style="color:red"></small>
                            </div>
                        </div>


                        <div class="row verify-login-btn d-none">
                            <div class="col-12 ">
                                <button class="btn btn-light swipe-to-top" id="verifylogin">Verify OTP</button>

                            </div>
                        </div>

                        <div class="row acc-login-btn">
                            <div class="col-12 ">
                                <button class="btn btn-secondary swipe-to-top" id="loginAccount">{{ trans('lables.login-login') }}</button>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6">

                <div class="heading">
                    <h2>
                        {{ trans('lables.login-create-account') }}
                    </h2>
                    <hr>
                </div>
                <div class="col-12 registration-process mb-0">

                    <form id="registerForm">
                        <div class="row">
                            <div class="from-group mb-3 col-12 col-md-6">
                                <label for="inlineFormInputGroup">{{ trans('lables.login-first-name') }}</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="registerFirstName" placeholder="{{ trans('lables.login-first-name') }}">
                                </div>
                                <small class="first_name errors d-none" style="color:red"></small>
                            </div>
                            <div class="from-group mb-3 col-12 col-md-6">
                                <label for="inlineFormInputGroup">{{ trans('lables.login-last-name') }}</label>
                                <div class="input-group">

                                    <input type="text" class="form-control" id="registerLastName" placeholder="{{ trans('lables.login-last-name') }}">
                                </div>
                                <small class="last_name errors d-none" style="color:red"></small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="from-group mb-3 col-12">
                                <label for="inlineFormInputGroup">{{ trans('lables.login-email') }}</label>
                                <div class="input-group">

                                    <input type="text" class="form-control" id="registerEmail" placeholder="{{ trans('lables.login-email') }}">
                                </div>
                                <small class="email errors d-none" style="color:red"></small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="from-group mb-3 col-12">
                                <label for="inlineFormInputGroup">Mobile</label>
                                <div class="input-group">

                                    <input type="text" class="form-control" id="mobile" placeholder="Enter mobile number">
                                </div>
                                <small class="mobile errors d-none" style="color:red"></small>
                            </div>
                        </div>



                        <div class="row d-none otp-row">
                            <div class="from-group mb-3 col-12">
                                <label for="inlineFormInputGroup">OTP</label>
                                <div class="input-group">

                                    <input type="text" class="form-control" id="otp" placeholder="Enter otp sent to your mobile number">
                                </div>
                                <small class="otp errors d-none" style="color:red"></small>
                            </div>
                        </div>

                        <div class="row acc-btn">
                            <div class="col-12 ">
                                <button class="btn btn-light swipe-to-top" id="createAccount">{{ trans('lables.login-create-account') }}</button>

                            </div>
                        </div>

                        <div class="row verify-btn d-none">
                            <div class="col-12 ">
                                <button class="btn btn-light swipe-to-top" id="verify">Verify OTP</button>

                            </div>
                        </div>




                    </form>
                </div>
            </div>
            
             
            </div>
        </div>
    </section>
    <!-- shop-page-section end -->

   
@include('theme.cmn_footer')

<script>
    loggedIn = $.trim(localStorage.getItem("customerLoggedin"));
    if(loggedIn == '1'){
        window.location.href = "{{url('/')}}";
    }

    $("#createAccount").click(function(e) {
        e.preventDefault();
        creatAcount();
    });

    $("#verify").click(function(e) {
        e.preventDefault();
        verifyOtp();
    });

    $("#verifylogin").click(function(e) {
        e.preventDefault();
        verifylogin();
    });

    $(".google-click").click(function() {
        localStorage.setItem("sociallite",'google');
    });

    $(".facebook-click").click(function() {
        localStorage.setItem("sociallite",'facebook');
    });

    @if(isset($_GET["code"]) && $_GET["code"] != '')
        code = "{{$_GET['code']}}"
        scope = "{{$_GET['scope']}}"
        authuser = "{{$_GET['authuser']}}"
        prompt = "{{$_GET['prompt']}}"
        sociallite = localStorage.getItem("sociallite");
        $.ajax({
            type: 'get',
            url: "{{ url('') }}" + '/api/client/customer_login/'+sociallite+'/callback?code='+code+'&scope='+scope+'&authuser='+authuser+'&prompt='+prompt,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                clientid: "{{isset($setting['client_id']) ? $setting['client_id'] : ''}}",
                clientsecret: "{{isset($setting['client_secret']) ? $setting['client_secret'] : ''}}",
            },
            beforeSend: function() {},
            success: function(data) {
                if(data.status == 'Success'){
                    localStorage.setItem("customerToken",data.data.token);
                    localStorage.setItem("customerHash",data.data.hash);
                    localStorage.setItem("customerId",data.data.id);
                    localStorage.setItem("customerLoggedin",'1');
                    localStorage.setItem("cartSession",'');
                    window.location.href = "{{ URL('/')}}";
                }
            },
            error: function(data) {
                console.log(data.responseJSON.errors);
                if(data.status == 422){
                    $.each( data.responseJSON.errors, function( index, value ){
                        $("#registerForm").find("."+index).html(value)
                        $("#registerForm").find("."+index).removeClass('d-none');
                    });
                }
            },
        });
    @endif

    

    function verifyOtp(){

        otp = $("#otp").val();
        mobile = $("#mobile").val();
        $(".errors").addClass('d-none');
        customerLogin = $.trim(localStorage.getItem("customerLoggedin"));
        /*if(customerLogin == '1'){
            toastr.error('{{ trans("already-logged-in") }}');
            return;
        }*/

        cartSession = $.trim(localStorage.getItem("cartSession"));
        if(cartSession == null || cartSession == 'null'){
            cartSession = '';
        }
        
        $.ajax({
            type: 'post',
            url: "{{ url('') }}" + '/api/client/verify',
            data:{
                otp: otp,
                mobile:mobile,
                session_id:cartSession,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                clientid: "{{isset($setting['client_id']) ? $setting['client_id'] : ''}}",
                clientsecret: "{{isset($setting['client_secret']) ? $setting['client_secret'] : ''}}",
            },
            beforeSend: function() {},
            success: function(data) {
                if(data.status == 'Success'){

                    localStorage.setItem("customerToken",data.data.token);
                    localStorage.setItem("customerHash",data.data.hash);
                    localStorage.setItem("customerId",data.data.id);
                    localStorage.setItem("customerLoggedin",'1');
                    localStorage.setItem("cartSession",'');
                    window.location.href = "{{ URL('/')}}";
                }else{
                    $(".otp").html('Enter valid otp');
                    $(".otp").removeClass('d-none');

                }
            },
            error: function(data) {
                console.log(data.responseJSON.errors);
                if(data.status == 422){
                    $.each( data.responseJSON.errors, function( index, value ){
                        $("#registerForm").find("."+index).html(value)
                        $("#registerForm").find("."+index).removeClass('d-none');
                    });
                }
            },
        });
    }
    function creatAcount() {
        firstname = $("#registerFirstName").val();
        lastname = $("#registerLastName").val();
        email = $("#registerEmail").val();
        otp = $("#otp").val();
        mobile = $("#mobile").val();
        $(".errors").addClass('d-none');
        customerLogin = $.trim(localStorage.getItem("customerLoggedin"));
        if(customerLogin == '1'){
            toastr.error('{{ trans("already-logged-in") }}');
            return;
        }

        cartSession = $.trim(localStorage.getItem("cartSession"));
        if(cartSession == null || cartSession == 'null'){
            cartSession = '';
        }
        
        $.ajax({
            type: 'post',
            url: "{{ url('') }}" + '/api/client/customer_register',
            data:{
                first_name: firstname,
                last_name: lastname,
                email: email,
                password: otp,
                mobile:mobile,
                session_id:cartSession,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                clientid: "{{isset($setting['client_id']) ? $setting['client_id'] : ''}}",
                clientsecret: "{{isset($setting['client_secret']) ? $setting['client_secret'] : ''}}",
            },
            beforeSend: function() {},
            success: function(data) {
                if(data.status == 'Success'){
                    $(".otp-row").removeClass('d-none');
                    $(".verify-btn").removeClass('d-none');
                    $(".acc-btn").addClass('d-none');

                    $("#registerFirstName").attr('readonly',true);
                    $("#registerLastName").attr('readonly',true);
                    $("#registerEmail").attr('readonly',true);
                    $("#mobile").attr('readonly',true);

                    //window.location.href = "{{ URL('/')}}";
                }
            },
            error: function(data) {
                console.log(data.responseJSON.errors);
                if(data.status == 422){
                    $.each( data.responseJSON.errors, function( index, value ){
                        $("#registerForm").find("."+index).html(value)
                        $("#registerForm").find("."+index).removeClass('d-none');
                    });
                }
            },
        });
    }

    $("#loginAccount").click(function(e) {
        e.preventDefault();
        loginAcount();
    })

    function loginAcount() {
        email = $("#loginEmail").val();
        password = $("#loginPassword").val();
        $(".errors").addClass('d-none');
        customerLogin = $.trim(localStorage.getItem("customerLoggedin"));
        if(customerLogin == '1'){
            toastr.error('{{ trans("already-logged-in") }}');

            return;
        }

        cartSession = $.trim(localStorage.getItem("cartSession"));
        if(cartSession == null || cartSession == 'null'){
            cartSession = '';
        }
        
        $.ajax({
            type: 'post',
            url: "{{ url('') }}" + '/api/client/customer_login',
            data:{
                username: email,
                password: password,
                session_id:cartSession,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                clientid: "{{isset($setting['client_id']) ? $setting['client_id'] : ''}}",
                clientsecret: "{{isset($setting['client_secret']) ? $setting['client_secret'] : ''}}",
            },
            beforeSend: function() {},
            success: function(data) {
                if(data.status == 'Success'){
                    $(".otp-login-row").removeClass('d-none');
                    $(".verify-login-btn").removeClass('d-none');
                    $(".acc-login-btn").addClass('d-none');
                   // window.location.href = '{{ URL('/')}}';
                }
            },
            error: function(data) {
                console.log(data);
                if(data.status == 422){
                    if(data.responseJSON.status == 'Error'){
                        $("#loginForm").find(".password").html(data.responseJSON.message)
                        $("#loginForm").find(".password").removeClass('d-none');
                    }
                }
                if(data.status == 422){
                    $.each( data.responseJSON.errors, function( index, value ){
                        $("#loginForm").find("."+index).html(value)
                        $("#loginForm").find("."+index).removeClass('d-none');
                    });
                }
                
                    $(".loginemail").html("Enter registered email or mobile")
                    $(".loginemail").removeClass('d-none');
                
            },
        });
    }

    function verifylogin(){

        otp = $("#loginotp").val();
        mobile = $("#loginEmail").val();
        $(".errors").addClass('d-none');
        customerLogin = $.trim(localStorage.getItem("customerLoggedin"));
        /*if(customerLogin == '1'){
            toastr.error('{{ trans("already-logged-in") }}');
            return;
        }*/

        cartSession = $.trim(localStorage.getItem("cartSession"));
        if(cartSession == null || cartSession == 'null'){
            cartSession = '';
        }
        
        $.ajax({
            type: 'post',
            url: "{{ url('') }}" + '/api/client/verifylogin',
            data:{
                password: otp,
                mobile:mobile,
                session_id:cartSession,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                clientid: "{{isset($setting['client_id']) ? $setting['client_id'] : ''}}",
                clientsecret: "{{isset($setting['client_secret']) ? $setting['client_secret'] : ''}}",
            },
            beforeSend: function() {},
            success: function(data) {
                if(data.status == 'Success'){
                    localStorage.setItem("customerToken",data.data.token);
                    localStorage.setItem("customerHash",data.data.hash);
                    localStorage.setItem("customerLoggedin",'1');
                    localStorage.setItem("customerId",data.data.id);
                    localStorage.setItem("customerFname",data.data.first_name);
                    localStorage.setItem("customerLname",data.data.last_name);
                    localStorage.setItem("cartSession",'');
                    window.location.href = "{{ URL('/')}}";
                }else{
                    $(".loginotp").html('Enter valid otp');
                    $(".loginotp").removeClass('d-none');

                }
            },
            error: function(data) {
                console.log(data.responseJSON.errors);
                if(data.status == 422){
                    $.each( data.responseJSON.errors, function( index, value ){
                        $("#registerForm").find("."+index).html(value)
                        $("#registerForm").find("."+index).removeClass('d-none');
                    });
                }
                $(".loginotp").html('Enter valid otp');
                    $(".loginotp").removeClass('d-none');
            },
        });
    }
</script>


