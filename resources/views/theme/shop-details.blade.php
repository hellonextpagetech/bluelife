@include('theme.cmn_head')
@section('content')

@endsection
<style>
    .single-shop-block .image-box img {
    position: relative;
    display: inline-block;
    width: auto;
    transition: all 500ms ease;
    height: 250px;
}
.shop-details {
    position: relative;
    padding: 60px 0px 60px 0px;
}
.shop-details .product-details-content .content-box .price {
    position: relative;
    font-size: 25px;
    line-height: 40px;
    font-weight: 700;
    font-family: 'Playfair Display', serif;
    margin-bottom: 15px;
}
.shop-details .product-details-content .content-box .price {
    color: #00aecc;
}
.shop-details .product-details-content .content-box .price del {
    color: #a7a7a7;
    margin-left: 5px;
}
.item-quantity .input-group {
    height: 30px;
}
.shop-details .product-details-content .addto-cart-box .cart-btn button {
    position: relative;
    display: inline-block;
    font-size: 16px;
    font-weight: 700;
    color: #222;
    background: transparent;
    text-transform: uppercase;
    border-radius: 30px;
    text-align: center;
    padding: 5px 30px;
}
.shop-details .product-details-content .content-box .price,.shop-details .product-details-content .addto-cart-box{
    display: inline-flex;
    margin: 5px 0px;
}
.shop-details .item-quantity input.quantity-spinner {
    padding: 10px 15px !important;
    height: 40px;
}
.bootstrap-touchspin .input-group-btn-vertical > .btn.bootstrap-touchspin-up {
    position: absolute;
    background: transparent;
    padding: 0px 0px;
    left: 20px;
    top: 6px;
}
.bootstrap-touchspin .input-group-btn-vertical > .btn.bootstrap-touchspin-down {
    position: absolute;
    padding: 0px 0px;
    right: 20px;
    top: 6px;
}

.shop-details .item-quantity input.quantity-spinner {
    padding: 6px 2px !important;
    height: 40px;
    margin-left: 30px;
}
.shop-details .product-details-content .content-box .list-item li a {
    color: #FFF;
    padding: 5px;
}
.shop-details .product-details-content .image-box {
    position: relative;
    display: block;
    text-align: center;
    background: #fff;
    margin: 0px 20px;
    padding: 25px 0px;
    border-radius: 0;
    border: 1px solid #ccc;
    box-shadow: 1px -3px 7px 2px #ececec;
}
.text{
    text-align:left;
  }
.shop-details .product-details-content .related-pages .image-box{
   
    border: 0px solid #ccc;
    box-shadow: unset; 
}
.list-item{
    margin:3px;
}
.content-box .col-md-6{
    max-width: 49%;
    display: inline-block;
    padding-right: 0;
    padding-left: 0;
}
.shop-details .product-details-content {
    margin-bottom: 50px;
}
.shop-details .product-info-tabs .tab-btns li.active-btn, .shop-details .product-info-tabs .tab-btns li:hover {
    color: #fff;
    background: #00aecc;
    padding: 5px 25px;
    border-radius: 50px;
}
.shop-details .product-info-tabs .tab-btns li:before {
    position: absolute;
    content: '';
    background: unset;
}
.shop-details .product-info-tabs .tab-btns li {
    position: relative;
    display: inline-block;
    font-size: 20px;
        color: #000;
    font-weight: bold;
}
.shop-details .product-info-tabs .tabs-content .text p {
    position: relative;
    margin-bottom: 0;
}
.lower-content .image-column{
border: 1px solid #ececec;
}
.lower-content img{
width: 80px;
    height: 80px;
    margin: 10px auto;
    display: block;
}
.shop-details .related-product {
    position: relative;
    padding-top: 0;
}
.shop-details .product-details-content .content-box .text {
    position: relative;
    margin-bottom: 15px;
}
.page-title .content-box h1 {
    position: relative;
    font-size: 36px;
    line-height: 40px;
    font-weight: 700;
    color: #222;
    margin-bottom: 8px;
}
img:hover{
    cursor:pointer;
}
.multi-img figure{
    width: 110px;
    padding: 10px !important;
    margin-top: 10px !important;
     
}
.multi-img img{
    width: 110px;
    height: 110px;
}
.image-columns{
   margin-right: 35.5px !important; 
}
.image-column img{
    width: 285px;
    height: 300px;
}
.p-2{font-size: 14px; border: 2px solid #dcdada; width: 10%;}
.top-title .title-inner, .top-title .text-inner {
    position: relative;
    width: 100%;
    float: left;
}
.padding-0{
    padding:0px;
}
.single-shop-block .image-box img{
    height:200px;
}
.single-shop-block h3{
    text-align:left;
        margin-bottom: 5px;
}
.single-shop-block .inner-box{
    margin:10px 0px;
    padding:10px 20px;
}
.single-shop-block .price{
    margin-bottom:15px;
        text-align:left;

}
.shop-details .product-details-content .image-box{
    margin:0px;
    padding:0px;
}
.single-shop-block .text{
    margin-bottom:5px;
}
</style>
    <!--Page Title-->
    <section class="page-title centred" style="background-image: url({{ asset('theme/images/background/page-title.jpg')}});">
        <div class="auto-container">
            <div class="content-box">
                
            </div>
        </div>
    </section>
    <!--End Page Title-->


    <!-- shop-details -->
    <section class="shop-details">
        <div class="product-details-content">
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-12 col-sm-12 ">
                        <figure class="image-box image-column"><a href="images/bluelife/TulipsULTRA/TU.png" class="lightbox-image"><img src="images/bluelife/TulipsULTRA/TU.png" alt=""></a></figure>
                    <div class="row multi-img clearfix">
                        <div class="col-lg-2 col-md-3 col-sm-12 image-columns">
                            <figure class="image-box"><img onclick="changeLargeImg(this,'https://vote.localballot.in/bluelife/public/gallary/TU.png')" src="https://vote.localballot.in/bluelife/public/gallary/TU.png" alt=""></figure>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-12 image-columns">
                            <figure class="image-box"><img onclick="changeLargeImg(this,'https://vote.localballot.in/bluelife/public/gallary/news-4.jpg')" src="https://vote.localballot.in/bluelife/public/gallary/news-4.jpg" alt=""></figure>
                        </div>

                        <div class="col-lg-2 col-md-3 col-sm-12 image-columns">
                            <figure class="image-box "><img onclick="changeLargeImg(this,'https://vote.localballot.in/bluelife/public/gallary/service-1.jpg')" src="https://vote.localballot.in/bluelife/public/gallary/service-1.jpg" alt=""></figure>
                        </div>



                    </div>
                    <a class="btn btn-primary download" href=""><i class="fa fa-download"></i> Download Leaftet</a>
                        
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 content-columns">
                        <div class="content-box">
                            <h2 class="prd-title">Tulips Ultra</h2>
                            <div class="text prd-half-descs">Unique water purifier with Digital RO technology and detachable stainless steel storage tank.
Unique water purifier with Digital RO technology and detachable stainless steel storage tank.
</div>
                            <div class="col-md-12 padding-0">

                            <ul class="product-ids list-item">
                                <li>Product ID:</li>
                                <li class="product-id">3638</li>
                            </ul>

                            </div>
                            <div class="col-md-12 padding-0">
                            <div class="price">
                                MRP : 
                            </div>
                            
                            <div class="addto-cart-box clearfix">
                                <div class="item-quantity">
                                    <input class="quantity-spinner" id="quantity-input" type="text" value="1" name="quantity">
                                </div>
                                <div class="cart-btn">
                                    
                                </div>
                            </div>
                            </div>
                            
                            

                            <div class="quickview-peragraph ">
                                <h5 class="p-0 m-0 font-weight-bold mt-2">Key Features  </h5>
                                <table class="mt-2 table-responsive" style="line-height: 15px;">
                                    <tbody class="technical">
                                    <tr>
                                        <td class="p-2" style="font-size: 14px; border: 2px solid #dcdada; width: 10%;">Filter Cartridges</td>
                                        <td class="p-2" style="font-size: 14px; border: 2px solid #dcdada; width: 10%;">Polypropylene Sediment Filter, Silver Impregnated Granular Activated Pre-Carbon Filter, Silver Impregnated Fine Granular Activated Post-Carbon Filter</td>
                                    </tr>
                                    <tr>
    <td class="p-2" style="font-size: 14px; border: 2px solid #dcdada; width: 10%;">RO Membrane</td>
    <td class="p-2" style="font-size: 14px; border: 2px solid #dcdada; width: 10%;">Dow FilmTec TW30-1812-75/ Equivalent</td>
</tr>

<tr>
    <td class="p-2" style="font-size: 14px; border: 2px solid #dcdada; width: 10%;">UV Lamp</td>
    <td class="p-2" style="font-size: 14px; border: 2px solid #dcdada; width: 10%;">11 Watts, Philips</td>
</tr>

<tr>
    <td class="p-2" style="font-size: 14px; border: 2px solid #dcdada; width: 10%;">Purification Capacity</td>
    <td class="p-2" style="font-size: 14px; border: 2px solid #dcdada; width: 10%;">15 Litres/Hour**</td>
</tr>
<tr>
    <td class="p-2" style="font-size: 14px; border: 2px solid #dcdada; width: 10%;">Storage Tank Capacity</td>
    <td class="p-2" style="font-size: 14px; border: 2px solid #dcdada; width: 10%;">6 Litres, Stainless Steel </td>
</tr>

<tr>
    <td class="p-2" style="font-size: 14px; border: 2px solid #dcdada; width: 10%;">Minimum Inlet Water Pressure</td>
    <td class="p-2" style="font-size: 14px; border: 2px solid #dcdada; width: 10%;">0.3 Kg/cm2</td>
</tr>

<tr>
    <td class="p-2" style="font-size: 14px; border: 2px solid #dcdada; width: 10%;">Power Supply</td>
    <td class="p-2" style="font-size: 14px; border: 2px solid #dcdada; width: 10%;">24VDC, 2.5Amps.</td>
</tr>

                                    </tbody>
                                </table>
                           </div>
                            
                        </div>
                    </div>





                    <div class="col-md-4">
                        <div class="related-product">
            <div class="auto-container">
                <div class="top-title clearfix">
                    <div class="title-inner">
                        <h1>Related Products</h1>
                    </div>
                </div>
                <div class="row clearfix related-pages">
                    
                <div class="col-sm-12 shop-block">
                            <div class="single-shop-block">
                              <div class="inner-box">
                                <div class="row">
                                  <div class="col-md-5">
                                      <a href="https://bluelife.co.in/public/product/12/variable-product-2"><figure class="image-box">
                                      <img src="https://bluelife.co.in/public///gallary/tu1.png" alt=""></figure>
                                    </a>
                                  </div>
                                  <div class="col-md-7">
<h3><a href="https://bluelife.co.in/public/product/12/variable-product-2">Tulips Ultra</a></h3>
                                  <div class="text">Unique water purifier with Digital RO technology and detachable stainless..</div><div class="price">₹479.4 <del>₹940</del></div>
                                  
                                  </div>

                                </div>
                                

                                

                                </div>
                              </div>


                            </div>
                            <div class="col-sm-12 shop-block">
                            <div class="single-shop-block">
                              <div class="inner-box">
                                <div class="row">
                                  <div class="col-md-5">
                                      <a href="https://bluelife.co.in/public/product/12/variable-product-2"><figure class="image-box">
                                      <img src="https://bluelife.co.in/public///gallary/tu1.png" alt=""></figure>
                                    </a>
                                  </div>
                                  <div class="col-md-7">
<h3><a href="https://bluelife.co.in/public/product/12/variable-product-2">Tulips Ultra</a></h3>
                                  <div class="text">Unique water purifier with Digital RO technology and detachable stainless..</div><div class="price">₹479.4 <del>₹940</del></div>
                                  
                                  </div>

                                </div>
                                

                                

                                </div>
                              </div>


                            </div>
                </div>
            </div>
        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-info-tabs">
            <div class="border-shap">
                <div class="border-3" style="background-image: url(images/icons/border-3.png);"></div>
            </div>
            <div class="auto-container">
                <div class="product-tab tabs-box">
                    <ul class="tab-btns tab-buttons  clearfix">
                        <li class="tab-btn active-btn" data-tab="#tab-1">Salient Features</li>
                        <li class="tab-btn " data-tab="#tab-2">Technical Details</li>

                    </ul>
                    <div class="tabs-content">
                        <div class="tab active-tab clearfix" id="tab-1">
                            <div class="text prd-desc">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>
                                <p>Anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                            </div>
                        </div>
                        <div class="tab clearfix" id="tab-2">
                            <div class="text">
                                <div class="lower-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>
                                <p>Anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
            </div>
                            </div>
                        </div> 
                        <div class="tab clearfix" id="tab-3">
                            <div class="text reviews">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>
                                <p>Anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                            </div>
                        </div>              
                    </div>
                </div>
            </div>
        </div>
        
    </section>
    <!-- shop-details end -->
<footer class="main-footer">
        
        <div class="footer-upper footer-top">
            <div class="auto-container">
                <div class="widget-section wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="logo-widget footer-widget">
                                <figure class="footer-logo"><a href="index.php"><img src="{{ asset('theme/images/bluelife/logo-white.png')}}" alt=""></a></figure>
                                <div class="text">Our values are the key driving force those help us align the organisation towards customer sensitivity and deliver beyond customer's expectation.</div>
                            </div>
                            <div class="inner-box clearfix">
                    
                    <div class="footer-social pull-left">
                        <ul class="social-links clearfix"> 
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="links-widget footer-widget">
                                <h3 class="widget-title">Quick Links</h3>
                                <div class="widget-content">
                                    <ul class="list clearfix">
                                        <li><a href="{{ URL('page/about-us') }}">About Us</a></li>
                                        <li><a href="{{ URL('page/refund-policy') }}">Refund Policy</a></li>
                                        <li><a href="{{ URL('page/privacy-policy') }}">Privacy Policy</a></li>
                                        <li><a href="{{ URL('page/terms-and-conditions') }}">Terms & Conditions</a></li>
                                        <li><a href="{{ URL('contact-us') }}">Contact us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-4 col-md-6 col-sm-12 footer-column">
                            <div class="contact-widget footer-widget">
                                <h3 class="widget-title">Contact us</h3>
                                <div class="widget-content">
                                    <ul class="list clearfix">
                                        <li>BlueLife TechnoSciences India Pvt. Ltd, 
1-10-63&64, Chikoti Gardens,
Begumpet, <br />Hyderabad - 016.
Telangana, India.</li>
                                       <li>Call Us <a href="tel:9849067775">+91-98490 67775</a></li>
                                        <li>E-mail: <a href="mailto:info@bluelife.com">info@bluelife.com</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright" ><span style="float:left;">Copyrights &copy; 2021<a href="#"></a>. All rights reserved.</span> <span style="float:right;">Design & Developed by <a href="http://nextpagetechnologies.com/">Next Page Technologies Pvt Ltd</a></span></div>
            </div>
        </div>
    </footer>
    <!-- main-footer end -->





<!--Scroll to top-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fas fa-angle-up"></span>
</button>


<!-- jequery plugins -->
<script src="{{ asset('theme/js/jquery.js')}}"></script>
<script src="{{ asset('theme/js/popper.min.js')}}"></script>
<script src="{{ asset('theme/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('theme/js/owl.js')}}"></script>
<script src="{{ asset('theme/js/wow.js')}}"></script>
<script src="{{ asset('theme/js/validation.js')}}"></script>
<script src="{{ asset('theme/js/jquery.fancybox.js')}}"></script>
<script src="{{ asset('theme/js/scrollbar.js')}}"></script>
<script src="{{ asset('theme/js/jquery-ui.js')}}"></script> 
<script src="{{ asset('theme/js/appear.js')}}"></script>
<script src="{{ asset('theme/js/jquery.bootstrap-touchspin.js')}}"></script> 

<!-- map script -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyATY4Rxc8jNvDpsK8ZetC7JyN4PFVYGCGM"></script>
<script src="{{ asset('theme/js/gmaps.js')}}"></script>
<script src="{{ asset('theme/js/map-helper.js')}}"></script>

<!-- main-js -->
<script src="{{ asset('theme/js/script.js')}}"></script>

</body><!-- End of .page_wrapper -->

</html>


<script>
    var attribute_id = [];
    var attribute = [];
    var variation_id = [];
    var variation = [];
    $(document).ready(function() {
        fetchProduct();
        fetchRelatedProduct();
    });

    languageId = localStorage.getItem("languageId");
    if (languageId == null || languageId == 'null') {
        localStorage.setItem("languageId", '1');
        $(".language-default-name").html('Endlish');
        localStorage.setItem("languageName", 'English');
        languageId = 1;
    }

    customerToken = $.trim(localStorage.getItem("customerToken"));


    function fetchProduct() {
        var url = "{{ url('') }}" + '/api/client/products/' + "{{ $product }}" +
            '?getCategory=1&getDetail=1&language_id=' + languageId + '&currency=INR';
        var appendTo = 'product-page';
        $.ajax({
            type: 'get',
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
                clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
            },
            beforeSend: function() {},
            success: function(data) {
                if (data.status == 'Success') {
                    console.log(data);
                    var technical = "";
                    if(data.data.product_type=='variable'){
                      for(var i=0;i<data.data.attribute.length;i++){
        technical += '<tr><td class="p-2">'+data.data.attribute[i].attributes.detail[0].name+'</td><td class="p-2">'+data.data.attribute[i].variations[0].product_variation.detail[0].name+'</td></tr>';
                    }
                    $(".technical").html(technical);  
                    }
                    
                    var length = 500;
                    var imgpath = "{{ asset('')}}"+'/'+data.data
                            .product_gallary.detail[1].gallary_path;
                   $(".prd-title").html(data.data.detail[0].title);

                   $(".prd-half-desc").html(data.data.detail[0].desc.substring(0,length));
                   $(".prd-desc").html(data.data.detail[0].desc);
                    
                   $(".product-id").html(data.data.detail[0].product_id);


                   $(".category").html(data.data.category[0].category_detail.detail[0].name);


                   $(".price").html("₹"+data.data
                        .product_discount_price+"<del>"+"₹"+data.data
                        .product_price);
                    
                    if(data.data.reviews.length==0){
                        $(".reviews").html('No reviews');
                    }

                   var img = '<a href="'+imgpath+'" class="lightbox-image"><img src="'+imgpath+'" alt=""></a>';
                   $(".image-column").html(img);

                  var catrtadd =  '<a onclick="addToCart(this)" data-id="'+data.data.detail[0].product_id+'" data-type="simple" href="javascript:void(0)"><button type="button"><i class="flaticon-online-shop"></i>buy </button></a>';
                $(".cart-btn").html(catrtadd);                    

                }   
            },
            error: function(data) {},
        });
    }

function changeLargeImg(e,imgpath){
var img = '<a href="'+imgpath+'" class="lightbox-image"><img src="'+imgpath+'" alt=""></a>';
                   $(".image-column").html(img);
}
   
    function fetchRelatedProduct() {
        var url = "{{ url('') }}" + '/api/client/products?limit=10&getDetail=1&language_id=' + languageId + '&currency='+localStorage.getItem("currency");
        var appendTo = 'related';
        $.ajax({
            type: 'get',
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
                clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
            },
            beforeSend: function() {},
            success: function(data) {
                if (data.status == 'Success') {
                    var products = "";
console.log(data);
for (i = 0; i < data.data.length; i++) {
    if(data.data[i].product_id >5 && data.data[i].product_id <13){

        var length = 120;
var url = "{{ URL('/product/')}}";
var imgpath = "{{ asset('')}}"+'/'+data.data[i]
        .product_gallary.detail[1].gallary_path;
products += '<div class="col-lg-4 col-md-6 col-sm-12 shop-block"><div class="single-shop-block"><div class="inner-box"><div class="border-one"></div> <div class="border-two"></div><a href="'+url+'/'+
    data
    .data[i].product_id + '/' + data
    .data[i].product_slug+'"><figure class="image-box"><img src="'+imgpath+'" alt=""></figure></a>';
products += '<h3><a href="'+url+'/'+
    data
    .data[i].product_id + '/' + data
    .data[i].product_slug+'">'+data.data[i]
    .detail[0].title+'</a></h3><div class="text">'+data.data[i]
    .detail[0].desc.substring(0,length)+'..</div><div class="price">₹'+data.data[i]
    .product_discount_price+' <del>₹'+data.data[i]
    .product_price+'</del></div>';
products += '<div class="cart-btn"><a href="'+url+'/'+
    data
    .data[i].product_id + '/' + data
    .data[i].product_slug+'"><i class="flaticon-online-shop"></i>Add to Cart</a></div></div></div></div>';

    }
    

}
$(".related-page").html(products);
                }
            },
            error: function(data) {},
        });
    }

    function productReview() {
        rating = $("input[name=rating]").val();
        comment = $("#comment").val();
        title = $("#title").val();
        if(rating == ''){
            toastr.error('{{ trans("select-ratings") }}');
            return;
        }

        var url = "{{ url('') }}" + '/api/client/review?product_id={{ $product }}&comment=' + comment + '&rating=' + rating +'&title='+title;
        var appendTo = 'related';
        $.ajax({
            type: 'post',
            url: url,
            headers: {
                'Authorization': 'Bearer ' + customerToken,
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
                clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
            },
            beforeSend: function() {},
            success: function(data) {
                if (data.status == 'Success') {
                    toastr.success('{{ trans("rating-saved-successfully") }}');
                    $("#comment").val('');
                    $("#title").val('');
                    getProductReview();
                }
            },
            error: function(data) {
                console.log(data);
                if (data.status == 422) {
                    jQuery.each(data.responseJSON.errors, function(index, item) {
                        $("#" + index).parent().find('.invalid-feedback').css('display',
                            'block');
                        $("#" + index).parent().find('.invalid-feedback').html(item);
                    });
                }
                else if (data.status == 401) {
                    toastr.error('{{ trans("response.some_thing_went_wrong") }}');
                }
            },
        });
    }

    function getProductReview() {
        var url = "{{ url('') }}" + '/api/client/review?product_id={{ $product }}';
        $.ajax({
            type: 'get',
            url: url,
            headers: {
                'Authorization': 'Bearer ' + customerToken,
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
                clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
            },
            beforeSend: function() {},
            success: function(data) {
                if (data.status == 'Success') {
                    const temp2 = document.getElementById("review-rating-template");
                    $("#review-rating-show").html('');
                    for (review = 0; review < data.data.length; review++) {
                        const clone1 = temp2.content.cloneNode(true);
                        clone1.querySelector(".review-comment").innerHTML = data.data[review].comment;
                        clone1.querySelector(".review-date").innerHTML = data.data[review].date;
                        clone1.querySelector(".review-title").innerHTML = data.data[review].title;
                        if (data.data[review].rating == '5') {
                            clone1.querySelector(".review-rating5").setAttribute('checked', true);
                        } else if (data.data[review].rating == '4') {
                            clone1.querySelector(".review-rating4").setAttribute('checked', true);
                        } else if (data.data[review].rating == '3') {
                            clone1.querySelector(".review-rating3").setAttribute('checked', true);
                        } else if (data.data[review].rating == '2') {
                            clone1.querySelector(".review-rating2").setAttribute('checked', true);
                        } else if (data.data[review].rating == '1') {
                            clone1.querySelector(".review-rating1").setAttribute('checked', true);
                        }
                        $("#review-rating-show").append(clone1);
                    }
                }
            },
            error: function(data) {
                console.log(data);
            },
        });
    }


    function slideInital() {
        // Product SLICK
        // $('.slider-show').html('<div class="slider-for"></div><div class="slider-nav"></div>');
        // alert();
        jQuery('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            infinite: false,
            draggable: false,
            fade: true,
            asNavFor: '.slider-nav',
            reinit : true
        });
        jQuery('.slider-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            centerMode: true,
            centerPadding: '60px',
            dots: false,
            arrows: true,
            focusOnSelect: true,
            reinit : true
        });


        // Product vertical SLICK
        jQuery('.slider-for-vertical').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            infinite: false,
            draggable: false,
            fade: true,
            asNavFor: '.slider-nav-vertical'
        });
        jQuery('.slider-nav-vertical').slick({
            dots: false,
            arrows: true,
            vertical: true,
            asNavFor: '.slider-for-vertical',
            slidesToShow: 3,
            // centerMode: true,
            slidesToScroll: 1,
            verticalSwiping: true,
            focusOnSelect: true
        });

        jQuery(function() {
            // ZOOM
            jQuery('.ex1').zoom();

        });

    }
</script>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
  toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        loggedIn = $.trim(localStorage.getItem("customerLoggedin"));
        customerFname = $.trim(localStorage.getItem("customerFname"));
        customerLname = $.trim(localStorage.getItem("customerLname"));
        if (loggedIn != '1') {
            $(".auth-login").remove();
        } else {
            $(".without-auth-login").remove();
            $(".welcomeUsername").html(customerFname + " " + customerLname);
        }

        customerToken = $.trim(localStorage.getItem("customerToken"));


        languageId = localStorage.getItem("languageId");
        languageName = localStorage.getItem("languageName");

        if (languageName == null || languageName == 'null') {
            localStorage.setItem("languageId", $.trim("{{ $data['selectedLenguage'] }}"));
            localStorage.setItem("languageName", $.trim("{{ $data['selectedLenguageName'] }}"));
            $(".language-default-name").html($.trim("{{ $data['selectedLenguageName'] }}"));
            languageId = $.trim("{{ $data['selectedLenguage'] }}");
        } else {
            $(".language-default-name").html(localStorage.getItem("languageName"));
            $('.mobile-language option[value="' + localStorage.getItem("languageId") + '"]').attr('selected', 'selected');
        }

        currency = localStorage.getItem("currency");
        currencyCode = localStorage.getItem("currencyCode");
        if (currencyCode == null || currencyCode == 'null') {
            localStorage.setItem("currency", $.trim("{{ $data['selectedCurrency'] }}"));
            localStorage.setItem("currencyCode", $.trim("{{ $data['selectedCurrencyName'] }}"));
            $("#selected-currency").html($.trim("{{ $data['selectedCurrencyName'] }}"));
            currency = 1;
        } else {
            $("#selected-currency").html(localStorage.getItem("currencyCode"));
            $('.currency option[value="' + localStorage.getItem("languageId") + '"]').attr('selected', 'selected');
        }

        cartSession = $.trim(localStorage.getItem("cartSession"));
        if (cartSession == null || cartSession == 'null') {
            cartSession = '';
        }
        $(document).ready(function() {

            if (loggedIn != '1') {
                localStorage.setItem("cartSession", cartSession);
                menuCart(cartSession);
            } else {
                menuCart('');
            }

            //getWishlist();



        });

     function addToCart(input) {
            product_type = $.trim($(input).attr('data-type'));
            product_id = $.trim($(input).attr('data-id'));
            product_combination_id = '';
            if (product_type == 'variable') {
                if ($.trim($("#product_combination_id").val()) == '' || $.trim($("#product_combination_id").val()) ==
                    'null') {
                    toastr.error("{{ trans('response.select-combination') }}")
                    return;
                }
                product_combination_id = $("#product_combination_id").val();
            }

            qty = $.trim($("#quantity-input").val());
            if (qty == '' || qty == 'undefined' || qty == null) {
                qty = 1;
            }
            addToCartFun(product_id, product_combination_id, cartSession, qty);
        }

        function addToCartFun(product_id, product_combination_id, cartSession, qty) {
            if (loggedIn == '1') {
                url = "{{ url('') }}" + '/api/client/cart?session_id=' + cartSession + '&product_id=' + product_id +
                    '&qty=' + qty + '&product_combination_id=' + product_combination_id;
            } else {
                url = "{{ url('') }}" + '/api/client/cart/guest/store?session_id=' + cartSession + '&product_id=' +
                    product_id + '&qty=' + qty + '&product_combination_id=' + product_combination_id;
            }
            $.ajax({
                type: 'post',
                url: url,
                headers: {
                    'Authorization': 'Bearer ' + customerToken,
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
                    clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
                },
                beforeSend: function() {},
                success: function(data) {
                    if (data.status == 'Success') {
                        if (loggedIn != '1') {
                            localStorage.setItem("cartSession", data.data.session);
                            console.dir(data);
                            menuCart(data.data.session);
                        } else {
                            menuCart('');
                        }
                        alert('Product Added To Cart');
                        toastr.success('{{ trans('response.add-to-cart-success') }}')
                    } else if (data.status == 'Error') {

                        toastr.error('{{ trans('response.some_thing_went_wrong') }}');
                    }
                },
                error: function(data) {
                    console.log();
                    if (data.responseJSON.status == 'Error') {
                        // toastr.error(data.responseJSON.message);
                        toastr.error('{{ trans('response.some_thing_went_wrong') }}');
                    }

                },
            });
        }

        function menuCart(cartSession) {
            if (loggedIn == '1') {
                url = "{{ url('') }}" + '/api/client/cart?session_id=' + cartSession + '&currency=' + localStorage
                    .getItem("currency");
            } else {
                url = "{{ url('') }}" + '/api/client/cart/guest/get?session_id=' + cartSession + '&currency=' +
                    localStorage.getItem("currency");
            }
            $.ajax({
                type: 'get',
                url: url,
                headers: {
                    'Authorization': 'Bearer ' + customerToken,
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
                    clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
                },
                beforeSend: function() {},
                success: function(data) {
                    if (data.status == 'Success') {
                        $(".top-cart-product-show").html('');
                        
                        if (data.data.length > 0) {
                            
                            $(".cartcount").html(data.data.length);
                        } else {
                            $(".cartcount").html(0);
                        }
                    } else {
                        toastr.error('{{ trans('response.some_thing_went_wrong') }}');
                    }
                },
                error: function(data) {},
            });
        }


        $(document).on('click', '.quantity-plus', function() {
            var quantity = $('#quantity-input').val();
            $('#quantity-input').val(parseInt(quantity) + 1);
        })

        $(document).on('click', '.quantity-minus', function() {
            var quantity = $('#quantity-input').val();
            if (quantity > 1)
                $('#quantity-input').val(parseInt(quantity) - 1);
        });

        function removeCartItem(input) {

product_id = $.trim($(input).attr('data-id'));
product_combination_id = $.trim($(input).attr('data-combination-id'));
if (product_combination_id == null || product_combination_id == 'null') {
    product_combination_id = '';
}

if (loggedIn == '1') {
    url = "{{ url('') }}" + '/api/client/cart/delete?session_id=' + cartSession + '&product_id=' +
        product_id +
        '&product_combination_id=' + product_combination_id + '&language_id=' + languageId;
} else {
    url = "{{ url('') }}" + '/api/client/cart/guest/delete?session_id=' + cartSession + '&product_id=' +
        product_id + '&product_combination_id=' + product_combination_id + '&language_id=' + languageId;
}

$.ajax({
    type: 'DELETE',
    url: url,
    headers: {
        'Authorization': 'Bearer ' + customerToken,
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        clientid: "{{ isset(getSetting()['client_id']) ? getSetting()['client_id'] : '' }}",
        clientsecret: "{{ isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : '' }}",
    },
    beforeSend: function() {},
    success: function(data) {
        if (data.status == 'Success') {
            $(input).closest('tr').remove();
            cartItem(cartSession);
            menuCart(cartSession);
        } else {
            toastr.error('{{ trans('response.some_thing_went_wrong') }}');
        }
    },
    error: function(data) {},
});
}
        </script>


