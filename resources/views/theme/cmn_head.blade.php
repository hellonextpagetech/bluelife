<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<title>Bluelife - Best Indian Water Purifiers</title>

<!-- Fav Icon -->
<link rel="icon" href="{{ asset('theme/images/icons/bluelifefav.JPG')}}" type="image/x-icon">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&amp;display=swap" rel="stylesheet">

<!-- Stylesheets -->
<link href="{{ asset('theme/css/font-awesome-all.css')}}" rel="stylesheet">
<link href="{{ asset('theme/css/flaticon.css')}}" rel="stylesheet">
<link href="{{ asset('theme/css/owl.css')}}" rel="stylesheet">
<link href="{{ asset('theme/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{ asset('theme/css/jquery.fancybox.min.css')}}" rel="stylesheet">
<link href="{{ asset('theme/css/animate.css')}}" rel="stylesheet">
<link href="{{ asset('theme/css/imagebg.css')}}" rel="stylesheet">
<link href="{{ asset('theme/css/color.css')}}" rel="stylesheet">
<link href="{{ asset('theme/css/style.css')}}" rel="stylesheet">
<link href="{{ asset('theme/css/responsive.css')}}" rel="stylesheet">


<style>
    @font-face {
  font-family: FlamaBasic;
  src: url({{ asset('fonts/bluelife/FlamSemicondBasic.otf')}});
}

 @font-face {
  font-family: FlamaBasicBold;
  src: url({{ asset('fonts/bluelife/FlamSemicondBold.otf')}});
}
body,.main-menu .navigation > li > a,h1, h2, h3, h4, h5, h6,.text,.single-shop-block .price,.news-block-one .inner-box .lower-content .lower-box .btn-box a,.theme-btn{
    font-family:FlamaBasic !important;
}
body{
    font-weight:100 !important;
}
.about-img{
    margin-left: 60px;
}
.download{
    margin-top:15px;
}
.form-mg-top{
    margin-top: 85px;
}
.video-section {
    padding: 0 0px;
}
body {
    font-family: inherit;
}
.description h3{
    font-size: 1.3rem;
}
.pac-logo{
    z-index:10000 !important;
}
#pac-input{
    border: 1px solid #ececec;
    width: -webkit-fill-available;
    height: 40px;
}
.contact-section .inner-box .form-inner .form-group .error{
    color:red;
}
.cart-box{
    margin-top: -10px;

}
.cart-box a{
    position: relative;
    display: inline-block;
    font-size: 18px;
    color: #fff;
    width: 52px;
    height: 52px;
    line-height: 52px;
    text-align: center;
    border-radius: 50%;
    background: #346bae;
}
.cart-box a:hover {
    background: #00aecc;
}
.single-shop-block:hover .image-box img {
    filter: grayscale(100%);
    -webkit-filter: grayscale(0%);
    -moz-filter: grayscale(100%);
    -o-filter: grayscale(100%);
    -ms-filter: grayscale(100%);
    -webkit-transform: rotateY(
180deg);
    -moz-transform: rotateY(180deg);
    -ms-transform: rotateY(180deg);
    -o-transform: rotateY(180deg);
    transform: rotateY(
0deg);
}

.cart-box a span{
    background: #00aecc;
    position: absolute;
    top: 4px;
    right: -18px;
    width: 25px;
    height: 25px;
    line-height: 25px;
    text-align: center;
    font-size: 14px;
    font-weight: 700;
    color: #fff;
    font-family: 'PT Sans', sans-serif;
    border-radius: 50%;
    transition: all 500ms ease;
}

.cart-box a:hover span{
    background: #346bae;
}
.sticky-header .main-menu .navigation > li > a {
    padding: 5px 25px 5px 25px !important;
}
.sticky-header .main-menu .navigation > li.current > a, .sticky-header .main-menu .navigation > li:hover > a {
    background: #00aecc;
    border-radius: 20px;
    margin-top: 10px;
    color: #fff !important;
}
.sticky-header .main-menu .navigation > li.current > a{
    background: #346bae;
}
.main-menu .navigation > li > ul > li {
    position: relative;
    width: 100%;
    padding: 0px 20px;
    border-bottom: 1px solid #6f6d6d;
    }
    .home-slider.slider-style-03 .banner-video-area:after {
    background-color: rgb(0 0 0 / 17%) !important;
    }
    .main-menu .navigation > li > ul {
    position: absolute;
    left: 0px;
    top: 100%;
    width: 220px;
}
.sec-pad {
    padding: 50px 0px 60px 0px !important;
}
.main-menu .navigation > li > a{
    text-transform: capitalize;
    font-weight: 300;
}
.main-menu .navigation > li > ul > li:hover {
background: #00aecc;
    border-color: #00aecc;
}
.sticky-header .main-menu .navigation > li > a{
    margin-top: 10px;
}
.main-header.style-two .header-top .main-menu .navigation > li a {
    font-size: 14px;
    line-height: 20px !important;
}
.sticky-header .cart-box {
    margin-top: 5px !important;
}
#content_block_three .inner-box .form-inner .form-group .error{
    color:red;
}
.h-120{
    height:120px !important;
}
.hide{
    display:none;
}
.sendQuery {
    position: fixed;
    border-radius: 8px 8px 0 0;
    right: -370px;
    top: -70px;
    text-align: center;
    cursor: pointer;
    color: #fff;
    z-index: 9999999999999;
    background: #346bae;
    line-height: 8px;
    padding: 18px 10px;
    font-weight: 600;
    font-size: 18px;
    text-transform: capitalize;
    height: auto;
    transform: rotate(-90deg);
    -moz-transform: rotate(-90deg);
    -webkit-transform: rotate(-90deg);
    bottom: 0;
}

#exampleModal #content_block_three .inner-box {
    position: relative;
    background: #fff;
    padding: 0;
    border-radius: 5px;
    box-shadow: unset;
}
#content_block_three .inner-box .form-inner .form-group input[type='text'], #content_block_three .inner-box .form-inner .form-group input[type='email'], #content_block_three .inner-box .form-inner .form-group select {
    position: relative;
    width: 100%;
    height: 36px !important;
    background: unset !important;
    border: 1px solid #d1cfcf !important;
    border-radius: 30px;
    padding-top: 10px !important;
    padding-bottom: 10px !important;
    padding-left: 160px !important;
    padding-right: 50px !important;

    transition: all 500ms ease;
}
.single-shop-block .inner-box {
    position: relative;
    display: block;
    text-align: center;
    background: #fff;
    padding: 20px 20px;
    }
    .single-shop-block .image-box {
    position: relative;
    display: block;
    margin-top: 60px;
}
.main-header.style-two .outer-container {
    position: absolute;
    background:transparent;
    margin-top: 40px;
    box-shadow: unset;
    }
    .main-header.style-two .header-top .main-menu .navigation > li {
    padding-top: 5px;
}

.header-top .main-menu .navigation > li {
    z-index: 5;
}
.main-header.style-two .header-top .main-menu .navigation > li a{
    font-size: 14px;
    line-height: 30px;
    font-weight: 300;
}
.main-header.style-two .header-top {
    position: relative;
    background: #FFF;
    padding: 0px 0px 0px 0px;
}

#content_block_three .inner-box .form-inner .form-group .text-label {
    position: absolute;
    display: block;
    font-size: 16px;
    color: #222;
    /* margin-bottom: 8px; */
    background: #00aecc;
    margin-top: 0px;
    margin-left: 0px;
    padding: 5px 10px;
    border-radius: 30px;
    width: 30%;
}
.mr-168{
    margin-right:166px;
}

.testimonial-section {
    position: relative;
    padding: 30px 0px 30px 0px;
}
.testimonial-section .top-title {
    margin-bottom: 0;
}
.main-footer .footer-upper {
    position: relative;
    padding: 30px 0px 30px 0px;
}

.inline-block .col-md-4{
    display: inline-block;
        max-width: 32.333333%;
}
.delivery-section {
    position: relative;
    padding: 40px 0px;
}
.single-shop-block .image-box {
    position: relative;
    display: block;
    margin-top: 0;
    margin-bottom: 15px;
}
.single-shop-block h3 {
    margin-bottom: 5px;
}
.single-shop-block .text,.single-shop-block .price {
    margin-bottom: 10px;
}
.single-shop-block .cart-btn a,#enquiry-submit,.btn-box a,.theme-btn.style-one,.btn-secondary {
    background: #346bae !important; 
    border-color: #346bae !important;
    color:#FFF !important;
}
.single-shop-block .cart-btn a:hover,#enquiry-submit:hover,.btn-box a:hover,.theme-btn.style-one:hover,.btn-secondary:hover {
    background: #00aecc !important;
    border-color: #00aecc !important;
}
#enquiry-submit{
    width: 30% !important;
    float: right;
    padding: 5px 30px !important;
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1ke3DWt4PrbFACoFVYYhdUGx1g2ycoqk&amp;callback=initAutocomplete&amp;libraries=places&amp;v=weekly" defer=""></script>

</head>


<!-- page wrapper -->
<body class="boxed_wrapper">

    <!-- preloader -->
    <div class="preloader"></div>
    <!-- preloader -->

    <!-- main header -->
    <header class="main-header style-two">
        <!-- header-top -->
        <div class="header-top">
            <div class="auto-container clearfix">
                <div class="top-left pull-right mr-168">
                    
                    <nav class="main-menu navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                    
                                    <li class="dropdown"><a href="#">Customer Care</a>
                                        <ul>
                                            <li><a  data-toggle="modal" data-target="#exampleModal">Service Request</a></li>
                                            <li><a href="{{URL('shop?category=0')}}">Spare Parts</a></li>
                                            <li><a href="{{URL('contact-us')}}">Customer Care Contacts</a></li>
                                        </ul>
                                    </li>

                                    <li class="dropdown"><a href="#">Become a vendor</a>
                                        <ul>
                                            <li><a href="{{URL('dealers')}}">Dealer Locator</a></li>
                                            <li><a href="{{URL('shop?category=0')}}">Become a dealer</a></li>
                                        </ul>
                                    </li>

                                    <li class="dropdown"><a href="#">About Us</a>
                                        <ul>
                                            <li><a href="{{URL('shop')}}">About Bluelife</a></li>
                                            <li><a href="{{URL('shop?category=0')}}">HR Philosophy</a></li>
                                            <li><a href="{{URL('shop?category=0')}}">Become a vendor</a></li>
                                            <li><a href="{{URL('shop?category=0')}}">Corporate contacts</a></li>
                                            <li><a href="{{URL('shop?category=0')}}">Talent Development</a></li>
                                            <li><a href="{{URL('shop?category=0')}}">Vendor Registration</a></li>
                                            <li><a href="{{URL('shop?category=0')}}">Vision</a></li>
                                            <li><a href="{{URL('shop?category=0')}}">Work Culture</a></li>
                                            <li><a href="{{URL('shop?category=0')}}">Working With Bluelife</a></li>

                                        </ul>
                                    </li>

                                </ul>
                            </div>
                        </nav>
                </div>
            </div>
        </div><!-- header-top end -->
        <div class="outer-container">
            <div class="header-upper clearfix">
                <div class="upper-left pull-left clearfix">
                    <figure class="logo-box"><a href="{{URL('/')}}"><img src="{{ asset('theme/images/bluelife/logo-white.png')}}" alt=""></a></figure>
                    
                </div>
                <div class="upper-right pull-right clearfix">
                    <div class="menu-area pull-left">
                        <!--Mobile Navigation Toggler-->
                        <div class="mobile-nav-toggler">
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                        </div>
                        <nav class="main-menu navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                    <li class="current "><a href="{{URL('/')}}">Home</a>
                                        
                                    </li> 
                                   
                                    <li class="dropdown"><a href="#">Shop</a>
                                        <ul>
                                            <li><a href="{{URL('shop')}}">Water Purifiers</a></li>
                                            <li><a href="{{URL('shop?category=0')}}">Spare Parts</a></li>
                                        </ul>
                                    </li>

                                    
                                    <li ><a href="{{URL('blog')}}">Blog</a>
                                        
                                    </li>                              
                                    <li><a href="{{URL('faq')}}">Faq</a></li>
                                    <li><div class="cart-box">
                            <a href="{{URL('cart')}}">
                                <i class="flaticon-online-shop"></i>
                                <span class="cartcount"></span>
                            </a>
                            <a class="auth-login" style="margin-left:20px;" href="{{URL('profile')}}">
                                <i class="flaticon-user"></i>
                            </a>
                        </div></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div class="menu-right-content pull-left clearfix">
                        <div class="search-box-outer">
                            <div class="dropdown">
                                <button class="search-box-btn" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flaticon-magnifying-glass"></i></button>
                                <ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu3">
                                    <li class="panel-outer">
                                        <div class="form-container">
                                            <form method="get" action="{{URL('shop')}}">
                                                <div class="form-group">
                                                    <input type="search" name="search" value="" placeholder="Search...." required="">
                                                    <button type="submit" class="search-btn"><i class="fas fa-search"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <!--sticky Header-->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <figure class="logo-box"><a href="{{URL('/')}}"><img src="{{ asset('theme/images/bluelife/small-logo.png')}}" alt=""></a></figure>
                <div class="menu-area">
                    <nav class="main-menu clearfix">
                        
                    </nav>
                </div>

            </div>
        </div>
    </header>
    <!-- main-header end -->

    <!-- Mobile Menu  -->
    <div class="mobile-menu">
        <div class="menu-backdrop"></div>
        <div class="close-btn"><i class="fas fa-times"></i></div>
        
        <nav class="menu-box">
            <div class="nav-logo"><a href="index.php"><img src="{{ asset('theme/images/bluelife/logo.png')}}" alt="" title=""></a></div>
            <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
            <div class="contact-info">
                <h4>Contact Info</h4>
                <ul>
                    <li>102,Sai Nilayam, Cherlapalli, Hyderabad</li>
                    <li><a href="tel:+8801682648101">+91 9999999999</a></li>
                    <li><a href="mailto:info@bluelife.com">info@bluelife.com</a></li>
                </ul>
            </div>
            <div class="social-links">
                <ul class="clearfix">
                    <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                    <li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
                    <li><a href="#"><span class="fab fa-pinterest-p"></span></a></li>
                    <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                    <li><a href="#"><span class="fab fa-youtube"></span></a></li>
                </ul>
            </div>
        </nav>
    </div><!-- End Mobile Menu -->


    <!-- Button trigger modal -->

<div id="showHideForm" class="sendQuery"  data-toggle="modal" data-target="#exampleModal">Demo Request</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Demo Request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="content_block_three">
                        <div class="inner-box" style="margin-top:0px;">
                            <div class="form-inner">
                                 <form method="post"  id="enquiry-form" class="order-form">
                                    <div class="row clearfix">
                                        <div class=" col-md-12 col-sm-12 form-group">
                                            <label class="text-label">Name</label>
                                            <input type="text" name="name" onkeypress="return (event.charCode > 64 && 
event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" id="name" required="">
                                        </div>

                                        <div class=" col-md-12 col-sm-12 form-group" id="phone-row">
                                            <label class="text-label">Mobile</label>
                                            <input type="text" name="phone" maxlength="10"  onkeypress="return /\d/.test(String.fromCharCode(event.keyCode || event.which))" id="phone" required="">
                                            <label class='phone-error error'></label>
                                        </div>
                                        <div class=" col-md-12 col-sm-12 form-group">
                                            <label class="text-label">Email (Optional)</label>
                                            <input type="email" name="email" id="email" >
                                        </div>
                                        <div class=" col-md-12 col-sm-12 form-group">
                                            <label class="text-label">City</label>
                                            <input type="text" name="city" id="city" required="">
                                        </div>
                                        <div class=" col-md-12 col-sm-12 form-group" id="pincode-row">
                                            <label class="text-label">Pincode</label>
                                            <input type="text" name="pincode" onkeypress="return /\d/.test(String.fromCharCode(event.keyCode || event.which))" maxlength="6" id="pincode" required="">
                                            <label class='pincode-error error'></label>
                                        </div>
                                        <div class=" col-md-12 col-sm-12 form-group">
                                            <label class="text-label">Requesting For</label>
                                            
                                            <input type="text" value="demo" readonly name="request_for" >
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                            <div class="btn-box">
                                                <button type="submit" id="enquiry-submit">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                            
                           
      </div>


    </div>
  </div>
</div>


