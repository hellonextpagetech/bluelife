@include('theme.cmn_head')
@section('content')

@endsection
<style>
    .news-block-one .inner-box .lower-content .inner .info-box {
    position: relative;
    padding-bottom: 18px;
    margin-bottom: 0;
}
.news-block-one .inner-box .lower-content .inner .info-box:before {
    position: absolute;
    content: '';
    background: unset;
}
.news-block-one .inner-box .lower-content .inner .info-box li {
    position: relative;
    display: inline-block;
    font-size: 16px;
    margin: 5px;
    margin-right: 15px;
}
.news-block-one .inner-box .lower-content .inner {
    position: relative;
    margin-top: -70px;
    background: #fff;
    border-radius: 5px;
    padding: 25px 25px 25px 25px;
}
.sec-pad {
    padding: 0 0px 110px 0px !important;
}
.news-block-one .inner-box .lower-content .inner {
    position: relative;
    margin-top: 0;
    }
.srch{
    margin-bottom: 25px;
    margin-left: 10%;
    margin-top: 20px;
}
.inline{
    display:inline-block;
    background: #3f66f5;
    padding: 15px 50px;
    border-radius: 5px;
    margin: 0 25px;
}
.inline .btn{
    vertical-align: unset;
}
.inline .btn-primary {
    color: #fff;
    background-color: #346bae;
    border-color: #346bae;
}
.col-md-6{
    width:24%;
}
.form-control {
    display: inline;
    width: 70%;
}
</style>
    <!--Page Title-->
    <section class="page-title centred" style="background-image: url({{ asset('theme/images/background/page-title.jpg')}});">
        <div class="auto-container">
            <div class="content-box">
            </div>
        </div>
    </section>
    <!--End Page Title-->


    <!-- blog-grid -->
    <section class="blog-grid sec-pad">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-md-12 srch">
                    <div class="col-md-9 inline">
                        <input type="text" class="form-control" onkeypress="return /\d/.test(String.fromCharCode(event.keyCode || event.which))" id="srch_pincode" placeholder="Please enter your pincode to search" maxlength="6" />
                        <button type="button" class="btn btn-primary" onclick="return gotoDealers();">Search</button>
                        <button type="button" class="btn btn-primary" onclick="window.location.reload();">Reset</button>
                    </div>
                    
                    
                
                </div>
                <div class="row clearfix dealers_row">
                </div>

            </div>
        </div>
    </section>
    <!-- blog-grid end -->
    @include('theme.cmn_footer')
