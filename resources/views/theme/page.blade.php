
@include('theme.cmn_head')
@section('content')

@endsection
    <!--Page Title-->
    <section class="page-title centred" style="background-image: url({{ asset('theme/images/background/page-title.jpg')}});">
        <div class="auto-container">
            <div class="content-box">
            </div>
        </div>
    </section>
    <!--End Page Title-->
    <section class="pro-content">

<!-- Profile Content -->
<section class="profile-content">
    <div class="container">
        <div class="row">

            <div class="col-12 media-main">
            @if(isset($page->page_detail))
<br />
<h2>{{ $page->page_detail[0]->title }}</h2>

<br />
{!! $page->page_detail[0]->description !!}
@else
<h2>comming soon</h2>
@endif
            </div>
            
        </div>
    </div>
</section>
</section>

@include('theme.cmn_footer')
