@include('theme.cmn_head')
@section('content')

@endsection
    <!--Page Title-->
    <section class="page-title centred" style="background-image: url({{ asset('theme/images/background/page-title.jpg')}});">
        <div class="auto-container">
            
        </div>
    </section>
    <!--End Page Title-->


    <section class="about-section">
        
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                    <div id="content_block_two">
                        <div class="content-box">
                            <div class="sec-title"><h1>About Us</h1></div>
                            <div class="text">BlueLife takes pride in giving its people tremendous growth opportunity across various functions and markets both national and international. Our operations spans across India, BlueLife caters to all residential clean drinking water needs. BlueLife offers an excellent learning and developed environment. BlueLife strives to get the best out of its most valuable asset – it’s team. The team is provided a platform that encourages personal and professional development, cross functional learning and teamwork which enhances their passion, integrity and responsibility. Our dynamic, vibrant, open, fast-paced and challenging work environment brings out the best of every employee. We are a trend setting and disciplined organization with attrition lower than industry trends.
</div>
                            

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 inner-column">
                 <figure class="image-box wow flipInY about-img" data-wow-delay="600ms" data-wow-duration="1500ms"><a href="{{ asset('theme/images/bluelife/TulipsRED/highlights.png')}}" class="lightbox-image" data-fancybox="gallery"><img src="{{ asset('theme/images/bluelife/TulipsRED/highlights.png')}}" alt=""></a></figure>
   
                </div>
            </div>
        </div>
    </section>


    <!-- feature-section -->
    <section class="feature-section bg-color-1 sec-pad">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h1>Why we are special</h1>
            </div>
            <div class="inner-content">
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                        <div class="feature-block-one wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <div class="icon-box wow slideInDown" data-wow-delay="250ms" data-wow-duration="1500ms"><i class="flaticon-drop-leaf-table"></i></div>
                                <h3><a href="#">Maxium Purity</a></h3>
                                <div class="text">Exercitation ullamco laboris nisl aliquip duis aute irure dolor iny rep henderit voluptate velit.</div>
                                <div class="link"><a href="#">Read More</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                        <div class="feature-block-one wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <div class="icon-box wow slideInDown" data-wow-delay="250ms" data-wow-duration="1500ms"><i class="flaticon-water"></i></div>
                                <h3><a href="#">5 Steps Filtration</a></h3>
                                <div class="text">Exercitation ullamco laboris nisl aliquip duis aute irure dolor iny rep henderit voluptate velit.</div>
                                <div class="link"><a href="#">Read More</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                        <div class="feature-block-one wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <div class="icon-box wow slideInDown" data-wow-delay="250ms" data-wow-duration="1500ms"><i class="flaticon-teardrop"></i></div>
                                <h3><a href="#">Cholorine Free</a></h3>
                                <div class="text">Exercitation ullamco laboris nisl aliquip duis aute irure dolor iny rep henderit voluptate velit.</div>
                                <div class="link"><a href="#">Read More</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                        <div class="feature-block-one wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <div class="icon-box wow slideInDown" data-wow-delay="250ms" data-wow-duration="1500ms"><i class="flaticon-water-barrel"></i></div>
                                <h3><a href="#">Quality Certified</a></h3>
                                <div class="text">Exercitation ullamco laboris nisl aliquip duis aute irure dolor iny rep henderit voluptate velit.</div>
                                <div class="link"><a href="#">Read More</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- feature-section end -->

     <!-- service-layout-1 -->
     <section class="service-layout-1 sec-pad">
        <div class="auto-container">
            <div class="sec-title text-center">
                <h1>Our Services</h1>
            </div>
            <div class="row clearfix">
                <div class="col-lg-4 col-md-6 col-sm-12 service-block">
                    <div class="service-block-one wow flipInY" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/resource/service-1.jpg')}}" alt=""></a></figure>
                            <div class="lower-content">
                                <h3><a href="#">We Manufacture</a></h3>
                                <div class="text">Exercitation kamco sed laboris aliquip duis aute ure dolor laboret buica dolore magna aliqua duis aute irure.</div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 service-block">
                    <div class="service-block-one wow flipInY" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/resource/service-2.jpg')}}" alt=""></a></figure>
                            <div class="lower-content">
                                <h3><a href="#">We Sell</a></h3>
                                <div class="text">Exercitation kamco sed laboris aliquip duis aute ure dolor laboret buica dolore magna aliqua duis aute irure.</div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 service-block">
                    <div class="service-block-one wow flipInY" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/resource/service-3.jpg')}}" alt=""></a></figure>
                            <div class="lower-content">
                                <h3><a href="#">We Serve</a></h3>
                                <div class="text">Exercitation kamco sed laboris aliquip duis aute ure dolor laboret buica dolore magna aliqua duis aute irure.</div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- service-layout-1 end -->




    <section class="about-section bg-color-1">
        
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                    <div id="content_block_two">
                        <div class="content-box">
                            <div class="sec-title"><h1>Join us for dealers</h1></div>
                            <div class="text">Swift responsive, challenging and creating a fun filled environment - our employees describe the work culture at BlueLife. At your roles and responsibilities either in Corporate Office or in a Sales Office or in a Manufacturing Facility or in virtual meetings with another part of the world, or taking a coffee break with friends, you will always find yourself collaborating, sharing new ideas and working on BlueLife’s innovative, unique products and advanced technologies that impact people within India and around the globe. Working on products that have impact, you would be encouraged to take on further challenges all the time. </div>
                            <div class="text">BlueLife is a gathering of a diverse set of people with vast experiences, skills and passions, bound together by a common passion for Innovation, Uniqueness, Technology Integration and a belief in its tremendous impact on the society. Innovation and technology integration are integral to our vision, strategy and success. Our work culture recognizes and respects people with diverse perspectives and skill sets. BlueLife is aiming to become the top company of choice for people keen to build long term careers in sales and technology integrations.</div>
                            <div class="text">Our learning and development initiatives are tailored to help employees acquire technology advancements, engineering excellence, sales and marketing to be able to grow in a career path of their choice. The Excellence initiative is focused on raising awareness and improving their capability. 
</div>

                            <div class="bold-text">Got a question? Dive in …</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 inner-column">
                    <div id="content_block_three">
                        <div class="inner-box" style="margin-top:0px;">
                            <h2>Enquiry</h2>
                            <div class="form-inner">
                                <form method="post"  id="contact-form" class="order-form">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                            <label>Your Name</label>
                                            <input type="text" name="name" id="first_name" required="">
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 form-group">
                                            <label>Email</label>
                                            <input type="email" name="email" id="email" required="">
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12 form-group">
                                            <label>Phone</label>
                                            <input type="text" name="phone" id="phone" required="">
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                            <label>Address</label>
                                            <input type="text" name="address" id="address" required="">
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                            <label>Message</label>
                                            <input type="text" class="h-120"  name="message" id="message" required="">
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                            <div class="btn-box">
                                                <button type="submit" id="contact">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- video-section -->
    <section class="video-section  sec-pad">
        <div class="auto-container">
            <div class="upper-content">
                <div id="video_block_one">
                    <div class="video-inner">
                        <div class="video-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms" style="background-image: url({{ asset('theme/images/background/video-1.jpg')}});">
                            <div class="video-btn">
                                <a href="https://www.youtube.com/watch?v=nfP5N9Yc72A&amp;t=28s" class="lightbox-image" data-caption=""><i class="flaticon-play-button-arrowhead"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top-title clearfix">
                    <div class="title-inner">
                        <div class="sec-title"><h1>Helping To Improve</h1></div>
                    </div>
                    <div class="text-inner">
                        <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco exa consequat. Duis aute rue dolor prehendrit lorem ipsum sit amet consectetur adipisicing sed.</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- video-section end -->


    <!-- composition-section -->
    <section class="composition-section bg-color-1">
        
        <div class="auto-container">
            <div class="sec-title text-center">
                <h1>Bluelife Basic Water<br />Composition</h1>
            </div>
            <div class="upper-content">
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-12 col-sm-12 left-column">
                        <div class="inner-box">
                            <div class="single-item wow slideInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <figure class="icon-box">
                                    <span>K+</span>
                                    <img src="{{ asset('theme/images/icons/water-drop-1.png')}}" alt="">
                                </figure>
                                <h3><a href="#">Potassium</a></h3>
                                <h5>2.5 mg/L</h5>
                                <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                            </div>
                            <div class="single-item wow slideInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                                <figure class="icon-box">
                                    <span>Fl</span>
                                    <img src="{{ asset('theme/images/icons/water-drop-1.png')}}" alt="">
                                </figure>
                                <h3><a href="#">Fluoride</a></h3>
                                <h5>0.5 mg/L</h5>
                                <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 image-column">
                        <div class="image-box">
                            <div class="pattern-bg" style="background-image: url({{ asset('theme/images/icons/pattern-2.png')}});"></div>
                            <figure class="image wow slideInUp" data-wow-delay="0ms" data-wow-duration="1500ms"><img src="{{ asset('theme/images/bluelife/TulipsULTRA/TU.png')}}" alt=""></figure>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 right-column">
                        <div class="inner-box">
                            <div class="single-item wow slideInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <figure class="icon-box">
                                    <span>Cl</span>
                                    <img src="{{ asset('theme/images/icons/water-drop-1.png')}}" alt="">
                                </figure>
                                <h3><a href="#">Chloride</a></h3>
                                <h5>350a mg/L</h5>
                                <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                            </div>
                            <div class="single-item wow slideInRight" data-wow-delay="300ms" data-wow-duration="1500ms">
                                <figure class="icon-box">
                                    <span>Mg</span>
                                    <img src="{{ asset('theme/images/icons/water-drop-1.png')}}" alt="">
                                </figure>
                                <h3><a href="#">Magnesium</a></h3>
                                <h5>14.5 mg/L</h5>
                                <div class="text">Exercitation lamco laboris aliquip duis aute irure dolor rep...</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lower-content clearfix">
                <div class="single-item wow slideInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <h3>Nitrates</h3>
                    <h5>2 mg/L</h5>
                </div>
                <div class="single-item wow slideInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
                    <h3>Bicarbonates</h3>
                    <h5>157 mg/L</h5>
                </div>
                <div class="single-item wow slideInUp" data-wow-delay="400ms" data-wow-duration="1500ms">
                    <h3>Sulphates</h3>
                    <h5>5.6 mg/L</h5>
                </div>
                <div class="single-item wow slideInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <h3>Sodium</h3>
                    <h5>0.4 mg/L</h5>
                </div>
            </div>
        </div>
    </section>
    <!-- composition-section end -->


   

    <!-- testimonial-section  -->
    <section class="testimonial-section ">
        
        <div class="auto-container">
            <div class="top-title clearfix">
                <div class="title-inner">
                    <div class="sec-title"><h1>Our Testimonials</h1></div>
                </div>
                <div class="text-inner">
                    <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco exa consequat. Duis aute rue dolor prehendrit lorem ipsum sit amet consectetur adipisicing sed.</div>
                </div>
            </div>
            <div class="inner-content">
                <div class="client-testimonial-carousel owl-carousel owl-theme owl-dots-none">
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">Since vindictively over agile the some far well besides constructively well airy then close excellent grabbed gosh contrary far dalmatian upheld intrepid bought and toucan more some apart dear boa much cast falcon dwelled.</div>
                            <div class="author-info">
                                <h5 class="name">Brendon Taylor</h5>
                                <span class="designation">office delivery</span>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">Since vindictively over agile the some far well besides constructively well airy then close excellent grabbed gosh contrary far dalmatian upheld intrepid bought and toucan more some apart dear boa much cast falcon dwelled.</div>
                            <div class="author-info">
                                <h5 class="name">Brendon Taylor</h5>
                                <span class="designation">office delivery</span>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">Since vindictively over agile the some far well besides constructively well airy then close excellent grabbed gosh contrary far dalmatian upheld intrepid bought and toucan more some apart dear boa much cast falcon dwelled.</div>
                            <div class="author-info">
                                <h5 class="name">Brendon Taylor</h5>
                                <span class="designation">office delivery</span>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">Since vindictively over agile the some far well besides constructively well airy then close excellent grabbed gosh contrary far dalmatian upheld intrepid bought and toucan more some apart dear boa much cast falcon dwelled.</div>
                            <div class="author-info">
                                <h5 class="name">Brendon Taylor</h5>
                                <span class="designation">office delivery</span>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">Since vindictively over agile the some far well besides constructively well airy then close excellent grabbed gosh contrary far dalmatian upheld intrepid bought and toucan more some apart dear boa much cast falcon dwelled.</div>
                            <div class="author-info">
                                <h5 class="name">Brendon Taylor</h5>
                                <span class="designation">office delivery</span>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">Since vindictively over agile the some far well besides constructively well airy then close excellent grabbed gosh contrary far dalmatian upheld intrepid bought and toucan more some apart dear boa much cast falcon dwelled.</div>
                            <div class="author-info">
                                <h5 class="name">Brendon Taylor</h5>
                                <span class="designation">office delivery</span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Client Thumbs Carousel-->
                <div class="client-thumb-outer">
                    <div class="client-thumbs-carousel owl-carousel owl-theme owl-dots-none owl-nav-none">
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{ asset('theme/images/resource/testimonial-1.png')}}" alt=""></figure>
                        </div>
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{ asset('theme/images/resource/testimonial-2.png')}}" alt=""></figure>
                        </div>
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{ asset('theme/images/resource/testimonial-3.png')}}" alt=""></figure>
                        </div>
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{ asset('theme/images/resource/testimonial-1.png')}}" alt=""></figure>
                        </div>
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{ asset('theme/images/resource/testimonial-2.png')}}" alt=""></figure>
                        </div>
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{ asset('theme/images/resource/testimonial-3.png')}}" alt=""></figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- testimonial-section end -->


    <!-- clients-section  -->
    <section class="clients-section bg-color-1 sec-pad">
        <div class="auto-container">
            <div class="top-title clearfix">
                <div class="title-inner">
                    <div class="sec-title"><h1>Trusted Partners</h1></div>
                </div>
                <div class="text-inner">
                    <div class="text">Aliquaut enim mini veniam quis trud exercitation ullamco exa consequat. Duis aute rue dolor prehendrit lorem ipsum sit amet consectetur adipisicing sed.</div>
                </div>
            </div>
            <div class="clients-carousel owl-carousel owl-theme owl-nav-none owl-dots-none">
                <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/clients/client-1.png')}}" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/clients/client-2.png')}}" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/clients/client-3.png')}}" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/clients/client-4.png')}}" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="{{ asset('theme/images/clients/client-5.png')}}" alt=""></a></figure>
            </div>
        </div>
    </section>
    <!-- clients-section end -->
    @include('theme.cmn_footer')
<script>


  $("#contact-form").submit(function(e){
        e.preventDefault();
        $("#contact").attr('disabled',true);
        $("#contact").text('Submitting..');
        $('.invalid-feedback').css('display','none')
        first_name = $.trim($("#first_name").val());
        last_name = $.trim($("#address").val());
        email = $.trim($("#email").val());
        phone = $.trim($("#phone").val());
        message = $.trim($("#message").val());

        $.ajax({
        type: 'post',
        url: "{{ url('') }}" + '/api/client/contact-us',
        data:{
            first_name:first_name,
            last_name:last_name,
            email:email,
            phone:phone,
            message:message
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            clientid: "{{isset(getSetting()['client_id']) ? getSetting()['client_id'] : ''}}",
            clientsecret: "{{isset(getSetting()['client_secret']) ? getSetting()['client_secret'] : ''}}",
        },
        beforeSend: function() {},
        success: function(data) {
            if (data.status == 'Success') {
                alert('Email sent successfully.');
                toastr.error('{{ trans("response.contact-form-success") }}');
                window.location.reload();
            }
            else{
                $("#contact").attr('disabled',false);
                $("#contact").text('Submit');
                toastr.error('{{ trans("response.some_thing_went_wrong") }}');
            }
        },
        error: function(data) {
            // console.log(data);
            if(data.status == 422){
                jQuery.each(data.responseJSON.errors, function(index, item) {
                    $("#"+index).parent().find('.invalid-feedback').css('display','block');
                    $("#"+index).parent().find('.invalid-feedback').html(item);
                });
            }
            else{
                toastr.error('{{ trans("response.some_thing_went_wrong") }}');;
            }
            $("#contact").attr('disabled',false);
             $("#contact").text('Submit');

        },
        });
    });
</script>
