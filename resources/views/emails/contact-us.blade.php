@component('mail::message')
# New Contact Mail

@component('mail::table')
|        |          |
| ------------- | --------:|
| Name      | {{$data['first_name']}} {{$data['last_name']}}      |
| Email      | {{$data['email']}}      |
| Phone      | {{$data['phone']}}      |
| Message      | {{$data['message']}}      |
@endcomponent



Thanks,<br>
{{ config('app.name') }}
@endcomponent
