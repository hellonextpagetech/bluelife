@component('mail::message')
# Enquiry Mail

@component('mail::table')
|        |          |
| ------------- | --------:|
| Name      | {{$data['name']}}     |
| Email      | {{$data['email']}}      |
| Phone      | {{$data['phone']}}      |
| City      | {{$data['city']}}      |
| Pincode      | {{$data['pincode']}}      |
| Request For      | {{$data['request']}}      |

@endcomponent



Thanks,<br>
{{ config('app.name') }}
@endcomponent
