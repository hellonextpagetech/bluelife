@component('mail::message')
# Order Id - {{ $data[0]->order_id}}

<table class="table table-stripped">
	<thead>
		<th>Product Id</th>
		<th>Product Name</th>
		<th>Quantity</th>
		<th>Price</th>
	</thead>
	<tbody>
	@foreach($data as $order)
		<tr>
			<td>{{ $order->product_id }}</td>
			<td>{{ $order->title }}</td>
			<td>{{ $order->qty }}</td>
			<td>{{ $order->total }}</td>
		</tr>
	@endforeach
</tbody>
</table>


<br>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
