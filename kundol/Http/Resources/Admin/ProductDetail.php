<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Admin\Language as LanguageResource;


class ProductDetail extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'product_id' => $this->product_id,
            'title' => $this->title,
            'desc' => $this->desc,
            'related_products' => $this->related_products,
            'salient_features' => $this->salient_features,
            'techinal_details' => $this->techinal_details,
            'key_features' => $this->key_features,
            'leaftet' => $this->leaftet,
            'language' => new LanguageResource($this->language),
        ];
    }
}