<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Services\Web\HomeService;
use Illuminate\Http\Request;
class NotificationController extends Controller{
	/* Send SMS Method */
	public function sendSms($data){
		$msg = $data['message'];
		$api_key = config('app.sms_api_key');
		$contacts = $data['mobile'];
		$sender_id = config('app.sender_id');
		$sms_text = urlencode($msg);
		$template_id = config('app.otp_template_id');
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, "http://textbeam.in/api/v1/send-sms?api-key=".$api_key."&sender-id=".$sender_id."&sms-type=1&route=1&mobile=".$contacts."&message=".$sms_text."&te_id=".$template_id);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		$response = curl_exec($ch);
		curl_close($ch);
	}

}

