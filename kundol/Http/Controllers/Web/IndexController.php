<?php

namespace App\Http\Controllers\Web;
use App\Mail\ContactUs;

use App\Http\Controllers\Controller;	
use App\Services\Web\HomeService;
use Illuminate\Http\Request;
use App\Models\Admin\Product;
use App\Models\Admin\Attribute;
use App\Models\Admin\Brand;
use App\Models\Admin\BlogNews;
use App\Models\Admin\BlogCategory;
use App\Models\Admin\Page;
use App\Models\Web\Order;
use App\Models\Admin\Customer;
use App\Models\Admin\Setting;
use App\Mail\Order as OrderMail;

use Carbon\Carbon;
use DB;
use App\Http\Controllers\Web\NotificationController;
use App\Models\User;
use App\Repository\Web\CustomerAuthRepository;
use Auth;
use Illuminate\Support\Facades\Mail;
use App\Services\Admin\OrderService;



class IndexController extends Controller
{
    public function Index(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        // return $data['homeBanners'];
        $setting = getSetting();
        return view('theme.index',compact('data','setting'));
    }

    public function contactUs(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        return view('theme.contact',compact('data'));
    }

    

    public function aboutUs(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        return view('theme.aboutus',compact('data'));
    }
    public function faq(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        return view('theme.faq',compact('data'));
    }

    public function productDetail($product,$slug){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        return view('theme.shop-details',compact('data','product'));
    }

    public function shop(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $attribute = new Attribute;
        $languageId = $data['selectedLenguage'];
        $attribute = $attribute->getAttributeDetailByLanguageId($languageId);
        $attribute = $attribute->getVariationDetailByLanguageId($languageId);
        $attribute = $attribute->get();
        $brand = Brand::all();
        $data['attribute'] =$attribute ;
        $data['price_range'] =['0-500','500-1000','1000-2000'];
        $data['brand'] = $brand;

        $category = DB::table('categories')->leftjoin('category_detail','category_detail.category_id','categories.id')->where('category_detail.language_id','1')->get();
        $shopdata = [];
        foreach($category as $ctg){
            $products = DB::table('products')->leftjoin('product_category','product_category.product_id','products.id')->leftjoin('product_detail','product_detail.product_id','products.id')->where('product_detail.language_id','1')->where('product_category.category_id',$ctg->category_id)->get();
            if(count($products)>0){
                $result['category_name'] = $ctg->category_name;
                $result['products'] = $products;
                array_push($shopdata,$result);
            }
        }
        $data['shopdata'] = $shopdata;
        return view('theme.shop',compact('data'));
    }

    public function cartPage(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();
        
            
        return view('theme.cartpage', compact('data','setting'));
    }

    public function login(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();
        return view('theme.login', compact('data','setting'));
    }

    public function blogDetail($slug){
        
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();
        
        return view('blog.blog-detail', compact('data','setting','slug'));
    }

    public function blog(){
        
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();

        return view('theme.blog', compact('data','setting'));
    }

    


    public function checkout(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();

            
        return view('theme.checkout', compact('data','setting'));
    }

    public function wishlist(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();
        return view('wishlist', compact('data','setting'));
    }
    public function compare(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();
        return view('compare', compact('data','setting'));
    }

    public function profile(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();
        return view('theme.profile', compact('data','setting'));
    }

    public function thankyou($id){
       
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();
        DB::table('orders')->where('id',$id)->update(['order_status'=>'Paid']);
        $products = DB::table('order_detail')->leftjoin('product_detail','product_detail.product_id','order_detail.product_id')->where('product_detail.language_id','1')->where('order_id',$id)->get();

        
        Mail::to('niranjanpusuluri@gmail.com')->send(new OrderMail($products));
        
        return view('theme.thankyou', compact('data','setting'));
    }


    public function changePassword(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();
        return view('theme.change-password', compact('data','setting'));
    }


    public function shippingAddress(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();
        return view('theme.shipping-address', compact('data','setting'));
    }

    public function orders(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();
        return view('theme.orders', compact('data','setting'));
    }

    public function ordersDetail($id){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();
        return view('theme.order-detail', compact('data','setting','id'));
    }
    
    public function term(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();
        return view('term', compact('data','setting'));
    }

    public function refund(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();
        return view('refund', compact('data','setting'));
    }

    public function privacy(){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();
        return view('privacy', compact('data','setting'));
    }
    public function sendContactMail(Request $request){

        $data = ['first_name' => $request->name, 'last_name' => $request->address, 'email' => strtolower($request->email), 'message' => $request->message, 'phone' => $request->phone];
        
        $setting = Setting::where('type', 'email_notify_setting')->pluck('value', 'key');
        $senderEmail = explode(',',$setting['notify_email']);
        foreach($senderEmail as $email){
            Mail::to($email)->send(new ContactUs($data));
        }

        return redirect()->back()->with('message','Message sent successfully.');
    }

    public function dealers(Request $request){
       /* if ($distanceIn == 'km') {
                $results = self::select(['*', DB::raw('( 0.621371 * 3959 * acos( cos( radians('.$lat.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians(lat) ) ) ) AS distance')])->havingRaw('distance < '.$distance)->get();
            } else {
                $results = self::select(['*', DB::raw('( 3959 * acos( cos( radians('.$lat.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians(lat) ) ) ) AS distance')])->havingRaw('distance < '.$distance)->get();
            }*/
$lat = $request->latitude;
$lng = $request->longitude;
$distance = 25;
if($lat!="" && $lat!=null){
$getDealers = DB::table('dealers')
                ->selectRaw(DB::raw('( 0.621371 * 3959 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians(latitude) ) ) ) AS distance,org_name,id,mobile,email'))->orderby('distance','ASC')->get();
}else{
    $getDealers = DB::table('dealers')->get();
}
       
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();
        $dealers = $getDealers;
        return view('theme.dealers', compact('data','setting','dealers'));
    }

    public function srchDealers(Request $request){
        $lat = $request->lat;
        $lng = $request->lng;
        $distance = 1;
        if($lat!="" && $lat!=null){
            $getDealers = DB::table('dealers')
                ->selectRaw(DB::raw('( 0.621371 * 3959 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians(latitude) ) ) ) AS distance,org_name,id,mobile,email'))->havingRaw('distance < '.$distance)->orderby('distance','ASC')->get();
        }
        return $getDealers;
    }


    public function page($slug){
        $homeService = new HomeService;
        $data = $homeService->homeIndex();
        $setting = getSetting();

        $languageId = $data['selectedLenguage'];

        $page = new Page;
        $page = $page->getPageDetailByLanguageId($languageId);
        $page = $page->where('slug',$slug);
        $page = $page->first();

        // return $page;
        return view('theme.page', compact('data','setting','page'));
    }


    public function orderStats(){

        $totalOrders = Order::all();
        $thisYearOrders = Order::whereYear('created_at', date('Y'))->get();
        $lastYearOrders = Order::whereYear('created_at', now()->subYear()->year)
        ->get();
        $thisWeekOrders = Order::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();


        $totalProducts = Product::all();
        $thisYearProducts = Product::whereYear('created_at', date('Y'))->get();
        $lastYearProducts = Product::whereYear('created_at', now()->subYear()->year)
        ->get();
        $thisWeekProducts = Product::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();


        $totalCustomers = Customer::all();
        $thisYearCustomers = Customer::whereYear('created_at', date('Y'))->get();
        $lastYearCustomers = Customer::whereYear('created_at', now()->subYear()->year)
        ->get();
        $thisWeekCustomer = Customer::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();

        $totalSales = Order::where('order_status','Complete')->sum('order_price');
        $thisYearSales = Order::where('order_status','Complete')->whereYear('created_at', date('Y'))->sum('order_price');
        $lastYearSales = Order::where('order_status','Complete')->whereYear('created_at', now()->subYear()->year)
        ->sum('order_price');

        $thisWeekSales = Order::where('order_status','Complete')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('order_price');

        $customerMonthly = Customer::select(DB::raw('count(id) as `id`'), DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"),  DB::raw('MONTHNAME(created_at) month') , DB::raw('MONTH(created_at) monthNumber'))
        ->groupby('month')
        ->whereYear('created_at', date('Y'))
        ->orderBy('monthNumber','ASC')
        ->get();



        $saleMonthly = Order::select(DB::raw('sum(order_price) as `amount`'), DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"),  DB::raw('MONTHNAME(created_at) month') , DB::raw('MONTH(created_at) monthNumber'))
        ->where('order_status','Complete')
        ->groupby('month')
        ->whereYear('created_at', date('Y'))
        ->orderBy('monthNumber','ASC')
        ->get();


        return [
            'totalOrders' => count($totalOrders),'thisYearOrders' =>count($thisYearOrders),'lastYearOrders' => count($lastYearOrders),
            'totalProducts' => count($totalProducts),'thisYearProducts' =>count($thisYearProducts),'lastYearProducts' => count($lastYearProducts),
            'totalCustomers' => count($totalCustomers),'thisYearCustomers' =>count($thisYearCustomers),'lastYearCustomers' => count($lastYearCustomers),'lastYear'=>now()->subYear()->year,

            'totalSales' =>$totalSales ,'thisYearSales' =>$thisYearSales,'lastYearSales' => $lastYearSales,
            'thisWeekOrders'=>$thisWeekOrders,'thisWeekProducts'=>$thisWeekProducts,'thisWeekCustomer'=>$thisWeekCustomer,
            'thisWeekSales'=>$thisWeekSales,'customerMonthly'=>$customerMonthly,'saleMonthly'=>$saleMonthly

        ];
    }
}


