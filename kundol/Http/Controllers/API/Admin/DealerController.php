<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Services\Web\HomeService;
use Illuminate\Http\Request;
use App\Models\Admin\Dealer as Dealer;
use App\Http\Requests\DealerStoreRequest;



class DealerController extends Controller{
	/* Get Dealers List */
	public function listDealers(Request $request){
		$getDealerData = Dealer::get();
		if(count($getDealerData)>0){
			return response()
            ->json([
                'status' => 'Success',
                'data' => $getDealerData,
            ], 200);
		}else{
			return response()
            ->json([
                'status' => 'Fail',
                'data' => [],
            ], 200);
		}
		
	}

	public function addDealers(Request $request){
		$data = $request->all();
		$insert_dealer_response = Dealer::create($data);
		if($insert_dealer_response){
			return response()
            ->json([
                'status' => 'Success',
                'data' => [],
            ], 200);
		}else{
			return response()
            ->json([
                'status' => 'Fail',
                'data' => [],
            ], 200);
		}
    }

    public function editDealers(Request $request,$id){
		$data = $request->all();
		$getDealerData = Dealer::where('id',$id)->get();
		if(count($getDealerData)>0){
			return response()
            ->json([
                'status' => 'Success',
                'data' => $getDealerData,
            ], 200);
		}else{
			return response()
            ->json([
                'status' => 'Fail',
                'data' => [],
            ], 200);
		}
    }

    public function updateDealers(Request $request,$id){
		$data = $request->all();
		$update_dealer_response = Dealer::updateOrCreate(
                    ['id' => $id],
                    $data
                );
		if($update_dealer_response){
			return response()
            ->json([
                'status' => 'Success',
                'data' => [],
            ], 200);
		}else{
			return response()
            ->json([
                'status' => 'Fail',
                'data' => [],
            ], 200);
		}
    }

    public function deleteDealer(Request $request,$id){
		$deleteDealer = Dealer::findOrFail($id);
        $deleteDealer->delete();
		if($deleteDealer){
			return response()
            ->json([
                'status' => 'Success',
                'data' => [],
            ], 200);
		}else{
			return response()
            ->json([
                'status' => 'Fail',
                'data' => [],
            ], 200);
		}
    }


}

