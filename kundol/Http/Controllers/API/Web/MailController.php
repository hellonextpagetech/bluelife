<?php

namespace App\Http\Controllers\API\Web;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests\ContactUsRequest;
use App\Mail\ContactUs;
use App\Mail\Contact;

use App\Models\Admin\Setting;
use Illuminate\Support\Facades\Mail;
use App\Traits\ApiResponser;

class MailController extends Controller
{
    use ApiResponser;
    public function contact_us(ContactUsRequest $request)
    {
        $data = ['first_name' => $request->first_name, 'last_name' => $request->last_name, 'email' => strtolower($request->email), 'message' => $request->message, 'phone' => $request->phone];
        
        Mail::to("customercare@bluelife.co.in")->send(new Contact($data));
        
        return $this->successResponse('', 'Email sent successfully!');
    }


    public function enquiry(ContactUsRequest $request)
    {
        $data = ['name' => $request->name, 'phone' => $request->phone, 'email' => strtolower($request->email), 'city' => $request->city, 'pincode' => $request->pincode,'request' => $request->request_for];
        
        if($request->request_for == "Demo"){
            Mail::to("sales@bluelife.co.in")->send(new ContactUs($data));
        }else{
            Mail::to("customercare@bluelife.co.in")->send(new ContactUs($data));
        }
        
        return $this->successResponse('', 'Email sent successfully!');
    }
}
