<?php

namespace App\Http\Controllers\API\Web;

use App\Contract\Web\CustomerAuthInterface;
use App\Http\Controllers\Controller as Controller;
use App\Http\Requests\CustomerLoginRequest;
use App\Http\Requests\CustomerStoreRequest;
use App\Http\Requests\ForgetRequest;
use App\Http\Requests\ResetRequest;
use App\Models\Admin\Customer;
use App\Models\User;
use App\Repository\Web\CustomerAuthRepository;
use Auth;
use Illuminate\Http\Request;
use Socialite;
use App\Http\Controllers\Web\NotificationController as Notify;

class CustomerAuthController extends Controller
{
    private $CustomerAuthRepository;

    public function __construct(CustomerAuthInterface $CustomerAuthRepository,Notify $Notify)
    {
        $this->Notify = $Notify;
        $this->CustomerAuthRepository = $CustomerAuthRepository;
    }

    public function register(CustomerStoreRequest $request)
    {
        
        $parms = $request->all();
        return $this->CustomerAuthRepository->store($parms);
    }

    public function verifyOtp(Request $request)
    {
        $parms = $request->all();
        return $this->CustomerAuthRepository->verify($parms);
    }

    public function forgetPassword(ForgetRequest $request)
    {
        $parms = $request->all();
        return $this->CustomerAuthRepository->forgetPassword($parms);
    }

    public function resetPassword(ResetRequest $request)
    {
        $parms = $request->all();
        return $this->CustomerAuthRepository->resetPassword($parms);
    }

    public function login(CustomerLoginRequest $request)
    {
        if (auth()->guard('customer')->user() || $request->cookie('_customer_token')) {
            return response()->json(['status' => 'Warning', "message" => "Already logged in", "_token" => $request->cookie('_token')], 200);
        }
        
        return $this->CustomerAuthRepository->login($request->username);

    }

    public function verifyLoginOtp(Request $request){
        $parms = $request->all();
        return $this->CustomerAuthRepository->verifylogin($parms);
    }

    public function Callback($provider)
    {
        $userSocial =   Socialite::driver($provider)->stateless()->user();
        $users =   Customer::where(['email' => $userSocial->getEmail()])->first();
        if ($users) {
            return $this->CustomerAuthRepository->loginWithProvider($users);
        } else {
            $user = Customer::create([
                'name'          => $userSocial->getName(),
                'email'         => $userSocial->getEmail(),
                'provider_id'   => $userSocial->getId(),
                'provider'      => $provider,
            ]);
            return $this->CustomerAuthRepository->loginWithProvider($user);
        }
    }

    public function redirect($provider)
    {
        return Socialite::driver($provider)->stateless()->redirect();
    }

    public function logout(Request $request)
    {
        $parms = $request->all();
        return $this->CustomerAuthRepository->logout($parms);
    }
}
