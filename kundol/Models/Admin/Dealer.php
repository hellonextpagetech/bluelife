<?php


namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Dealer extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    use SoftDeletes;
	
	protected $table = 'dealers';
    
    protected $fillable = [
        'org_name', 'contact_per_name', 'mobile', 'alt_mobile','landline','email','address','city','pincode','latitude','longitude','status',
    ];


    
}
